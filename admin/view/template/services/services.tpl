<?php echo $header;  ?>
<?php echo $column_left; ?>

<div id="content">
	
<div class="container-fluid">
	<div class="row">
			<div class="col-sm-12">
				<h2 class="pcolor"><strong>Services:</strong></h2>
			</div>
			<div class="col-sm-6 adminservice">
				<ul class="list-group">
				<?php foreach ($Showservices as $showser) { ?>
				
				<li class="list-group-item"><i class="fa fa-angle-double-right"></i> <?php echo $showser['service_list']; ?>
<a class="del" href="<?php echo $delete; ?>&del=<?php echo $showser['id']; ?>"><i data-toggle="tooltip" data-placement="right" title="delete this" class="fa fa-times-circle pull-right"></i></a> 
				</li>
					
				<?php } ?>
				</ul>
			</div>
			<div class="col-xs-6">

	    <form class="form-inline" action="<?php echo $action; ?>" id="form-hello" method="post" id="form-hello" enctype="multipart/form-data">
	    	<h3><strong>Add a service</strong></h3>
			  <p><em>Note: To Add a service, type it below</em></p>
			    <input type="text" id="addaservice" class="form-control" name="slist" placeholder="Eg: Computer" required>
			  	&nbsp;		  
			  <button style="margin-bottom:0px;" form="form-hello" type="submit" form="form-hello" data-toggle="tooltip" title="<?php // echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-plus-square"></i> Add Service</button>
			</form>
	    </div>

	</div>		
</div>






</div>

<?php echo $footer; ?>
