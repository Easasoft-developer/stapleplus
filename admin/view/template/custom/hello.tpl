<?php echo $header; echo $column_left; ?>
   <!--<ul class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul> -->

<div id="content">

    
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-xs-12" style="border-right: 1px solid rgb(238, 238, 238);">
            <form class="storeloc" action="<?php echo $action; ?>" method="post" id="form-hello" enctype="multipart/form-data">
              <fieldset>
                <h2 class="pcolor"><strong>Store information:</strong></h2>
                <label for="sname">Store name:</label><br>
                <input class="form-control" type="text" name="sname" id="sname" placeholder="Eg. Arab" required>
                <br>
                <label for="sadd">Store address:</label><br>
                <textarea class="form-control" name="sadd" id="sadd" placeholder="Eg. Address" required></textarea> 
                <br>
                <label for="saddress">City:</label><br>
                <input class="form-control" type="text" name="saddress" id="saddress" placeholder="eg. city" required>
                <br>
                <label for="code">Zip code/ place:</label><br>
                <input class="form-control" type="tel" maxlength="6" id="code" name="zipcode" placeholder="eg. 123456" required><br>
                <label>Latitude</label><br>
                <input class="form-control" id="Latitude" name="latitude" placeholder="" readonly><br>               
                <label>Longitude</label><br>
                <input class="form-control" id="Longitude" name="longitude" placeholder="" readonly><br>

                <p><em>Note:</em> <i class="fa fa-hand-o-down"></i> Find and drag to your store location in below map to find out above Latitude and Longitude.</p>         

              </fieldset>
              <button type="submit" form="form-hello" data-toggle="tooltip" title="<?php // echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            </form>
        </div>

        <div class="col-lg-8 col-md-8 col-xs-12">
        <div style="height:600px;width:100%;" id="map"> </div>
        
        </div>
    </div>
    <div class="row">
    <div class="store-location col-xs-12">
            <h2 class="pcolor"><strong>All Stores</strong></h2>
<?php //if(!empty($msg)){ echo $msg; } else { } ?>
              <?php $i=1; foreach ($showall as $showall) 
              {?>

              <address>
<?php echo  "<span><strong>Name:</strong> ".$showall['store_name']. 
'</span><br><span><strong>Address:</strong> '.$showall['store_address'].
'</span><br><span><strong>Zip code:</strong> '. $showall['zipcode'].
'</span><br><span><strong>City:</strong>'.$showall['city'].'</span><br>' ; ?>
 <a class="del" href="<?php echo  $delaction; ?>&store=<?php echo $showall['id']; ?>">Delete</a> </address>
            
              
            <?php
             $i++; }
             ?>
             </div></div>
</div>

</div> 





<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>

<script type="text/javascript">
  
 googleMap();
function googleMap() {


        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: new google.maps.LatLng(24.4667, 54.3667),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(24.4667, 54.3667),
            map: map,
            draggable: true,
            title: 'Hello World!'
        });

        google.maps.event.addListener(marker, 'dragend', function (evt) {

            geocodePosition(marker.getPosition());
            document.getElementById("Latitude").value = evt.latLng.lat();
            document.getElementById("Longitude").value = evt.latLng.lng();
        });

    }
    function geocodePosition(pos) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            latLng: pos
        }, function (responses) {
            if (responses && responses.length > 0) {
                console.log(responses[0].formatted_address);
                var add = responses[0].formatted_address;
                var str = add.split(',');
                var india = str[str.length - 1];
                var city = str[str.length - 2];
                var street = str;
                $('#Address_Zip').val();
                $('#Address_Street').val(street.toString());
                $('#Address_CityName').val(city);
                $('#Address_County').val(india);
//      updateMarkerAddress(responses[0].formatted_address);
            } else {

            }
        });
    }



</script>





<?php echo $footer; ?>
