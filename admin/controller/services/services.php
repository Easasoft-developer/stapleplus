<?php 

class ControllerServicesServices extends Controller
{
	public function index()
	{
	$this->load->model('services/services');

	$data['action'] = $this->url->link('services/services/Add', 'token=' . $this->session->data['token'] , 'SSL');

	$data['delete'] = $this->url->link('services/services/Del', 'token=' . $this->session->data['token'] ,'SSL');   

		 $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

         $data['Showservices'] = $this->model_services_services->Showservices();
        

        $this->response->setOutput($this->load->view('services/services.tpl', $data));

	}



	public function Add()

{

	$this->load->model('services/services');

	 if(($this->request->server['REQUEST_METHOD'] == 'POST')) {
        $this->model_services_services->Addservices($this->request->post);
}

	$this->response->redirect($this->url->link('services/services', 'token=' . $this->session->data['token'] , 'SSL'));

}





public function Del()

{


	$this->load->model('services/services');

	$del = $this->request->get['del'];
 	$this->model_services_services->Delservices($del);

	$this->response->redirect($this->url->link('services/services', 'token=' . $this->session->data['token'] , 'SSL'));



}






}

?>
