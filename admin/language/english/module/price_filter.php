<?php
// Heading
$_['heading_title']    = 'Price Range Filter';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Price Range filter module!';
$_['text_edit']        = 'Edit Price Range Filter Module';
$_['text_price_range'] = '0-300';

// Entry
$_['entry_status']     = 'Status';
$_['entry_price_range']= 'Price Range';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Price Range filter module!';