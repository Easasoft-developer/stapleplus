<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>

<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />

<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />

<link href="catalog/view/theme/newtheme/stylesheet/jquery.simplegallery.css" rel="stylesheet">
<link href="catalog/view/theme/newtheme/stylesheet/jquery.simplelens.css" rel="stylesheet">

<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>


<script type="text/javascript">
$(document).ready(function() {
$("#bs-example-navbar-collapse-1").hover(function(){
$.ajax({
type: "post",
url: "index.php?route=product/ajaxcall",
dataType: "json",
success: function(json) {
if(json['brands']) {

var fb = json.brands;
var html ='';
$.each(fb,function(i,ele){ 

html += '<div class="col-sm-3 col-xs-6">'+'<img src="image/'+ele.image+'" width="150" height="100">'+'</div>' 
});
$('.bimage').html(html);

}



else{
alert('error');
}


if (json['onefeatured']) {
var ofb = json.onefeatured;
var html2 = '';
$.each(ofb,function(j,ofele){ 
html2 += '<div class="onefeatured"><a href="index.php?route=product/product&product_id='+ofele.product_id+'"><img src="image/'+ ofele.image+'" class="img-responsive"></a></div><a href="index.php?route=product/product&path=27&product_id='+ofele.product_id+'"><button class="btn btn-default" type="submit">Shop Now<span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span></button></a>' 
});
$('.onefeatured').html(html2);
}


}



});
});
});
</script>


<script src="catalog/view/javascript/bootstrap/js/readmore.min.js" type="text/javascript"></script>
<link href="catalog/view/theme/newtheme/stylesheet/stylesheet.css" rel="stylesheet">
<link href="catalog/view/theme/newtheme/stylesheet/responsive.css" rel="stylesheet">
<link href="catalog/view/theme/newtheme/stylesheet/slick.css" rel="stylesheet">
<link href="catalog/view/theme/newtheme/stylesheet/slick-theme.css" rel="stylesheet">

			<script src="catalog/view/javascript/comboproducts.js" type="text/javascript"></script>
			
			<style type="text/css">
				.combo-section {
					width: 100%;
					border-top: 1px solid #4e4e4e;
				}
				
				.combo-section .combo-set {
					padding: 2px;
					width: 100%;
					min-height: 180px;
				}
				
				.combo-section .combo-set .combo-item {
					display: block;
					line-height: 14px;
					font-weight: bold;
					min-height: 14px;
					float: left;
					width: 14%;
				}
				
				.combo-item-img {	
					padding-right: 5px;
					padding-left: 5px;
					text-align: center;
				}
				
				.combo-item-name,.combo-item-price {
					text-align: center;
					font-size: small;
					  margin: 10px 0;
				}
				
				.combo-action {
					float:left;
					width: 17%;
				}
				
				.combo-plus, .combo-save {
					float: left;
					font-weight: bold;
					padding-left:10px;
				}
				.combo-save {float:none;}
				.combo-plus {
					line-height: 100px
				}
				
				.price_discount {
					color: #d44434;
				}
				
				.btn-combo {
					
				}
				
				.btn-combo-wishlist {
					  display: inline-block;
  padding: 6px 12px;
  margin-bottom: 0;
  font-size: 14px;
  font-weight: 400;
  line-height: 1.42857143;
  text-align: center;text-transform:uppercase;width: 100%;margin-top: 10px;
				}
				
				.btn-combo-cart {
				  display: inline-block;
  padding: 6px 12px;
  margin-bottom: 0;
  font-size: 14px;
  font-weight: 400;
  line-height: 1.42857143;
  text-align: center;
  text-transform: uppercase;
					  margin-top: 10px;
					  background: #46913e;border-color:#46913e;width: 100%;
				}
				.tooltip{background:transparent;}
				

				@media only screen and (max-width: 1199px) {
						.combo-action {width:21%;}
				}

				@media only screen and (max-width: 991px) {
						.combo-action {width:26%;}

						.combo-section .combo-set .combo-item {width:21%;}
				}

				@media only screen and (max-width: 767px) {
						.combo-action {width:50%;float:none;}

.combo-save {clear: both;padding-top: 20px;
padding-bottom: 20px;}
						.combo-section .combo-set .combo-item {width:31%;}
				}

				@media only screen and (max-width: 500px) {
					.combo-plus {
						display:none;
					}
					.combo-action {width: 100%;}
					.combo-action .btn-combo {
						width: 100%;
						height: 40px;
						display:block;
					}
					.combo-contain {min-height: 85px;}
					.combo-save {font-size: 90%;}
					
				}
				
			</style>

		
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>


<?php echo $google_analytics; ?>
</head>
<body>



<!-- <nav id="top">
  <div class="container">
    <?php echo $currency; ?>
    <?php echo $language; ?>
    <div id="top-links" class="nav pull-right">
      <ul class="list-inline">
        <li><a href="<?php echo $contact; ?>"><i class="fa fa-phone"></i></a> <span class="hidden-xs hidden-sm hidden-md"><?php echo $telephone; ?></span></li>
        <li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a>
          <ul class="dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
            <?php } ?>
          </ul>
        </li>
        <li><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_wishlist; ?></span></a></li>
        <li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span></a></li>
        <li><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_checkout; ?></span></a></li>
      </ul>
    </div>
  </div>
</nav> -->



<!-- ---------------------- -->
    <header>
      <div class="top">
        <div class="container">
          <div class="row">
              <div class="col-lg-5 col-md-5 col-sm-4 col-xs-12">
                <ul class="list-inline">
                  <li>Tel : +97 4441569</li>
                  <li>Email : <a href="mailto:info@stapleplus.ae">info@stapleplus.ae</a></li>
                </ul>
              </div>
             <div class="col-lg-6 col-lg-offset-1 col-md-7 col-sm-8 col-xs-12">
                <ul class="list-inline account">
                  <li><a href="index.php?route=corporate">Corporate/Government</a></li>
                  <li><a href="index.php?route=orderbyitem">Order by Item #</a></li>
                  <li><a href="<?php echo $order; ?>">Track Your Order</a></li>

                   <?php if (!$logged) { ?>
	<li class="dropdown"><a href="" class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Login or Register</a>

                    <div class="dropdown-menu login-dropdown">
                        <div class="col-md-6">
                          <h4>Login or Register</h4>  
            <form  action="<?php echo $action; ?>"  method="post" enctype="multipart/form-data">
              <div class="form-group">

                <input name="email" value="<?php echo $email; ?>" placeholder="E-Mail Address" id="input-email" class="form-control" type="text">
              </div>
              <div class="form-group">

                <input name="password" value="<?php echo $password; ?>" placeholder="Password" id="input-password" class="form-control" type="password"></div>
                <a href="<?php echo $forgotten; ?>">Forgotten Password</a>
<input type="submit" value="Sign In" class="btn btn-default buttons" href="<?php echo $button_login; ?>" />

                <?php if ($redirect) { ?>
              <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
              <?php } ?>
                          </form>
                        </div>                      
                        <div class="col-md-6">
                          <h4>Register</h4>
                          <p>Create an account to make your shopping experience easier.</p>
                          <a href="<?php echo $register; ?>" class="btn btn-default buttons">Register</a>
                        </div>
                    </div>
                  </li>
                 <?php } else { ?>
                <li><a href="<?php echo $logout; ?>">Logout</a></li>
                 <?php } ?>



                </ul>
            </div>
          </div>            
        </div>
        <!-- end of top container -->
      </div>




      <!--////////////////// end of top bar and start middle part ////////////////////// -->

 

      <div class="middle">
        
        <div class="container">
          <div class="row logo-srh-icn">

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
              <div id="logo">
          <?php if ($logo) { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>

            </div>

           <div class="col-lg-6 col-md-5 col-sm-4 col-xs-12 search">
              <h5><strong><i class="fa fa-truck"></i> <a href="index.php?route=information/information&information_id=12">Free Delivery On AED 250.00</a></strong></h5>
                  <form class="navbar-form" role="search">
                  <?php echo $search;?>  

                  </form>

            </div> 
             <div class="col-lg-3 col-md-4 col-sm-5 col-xs-12">
              <ul class="list-inline icons">
              <!-- stores-account-list-cart -->
                <li>
                <div class="st-acc-li-ca dropdown">
                <a class="icons-a"><img src="images/storespoint.png"/></a>
              <a class="dropdown-toggle" type="button" id="stores" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-map-marker visible-xs-inline"></i> Stores <span class="glyphicon glyphicon-chevron-down hidden-xs" aria-hidden="true"></span></a>

                  <div class="dropdown-menu find-store" aria-labelledby="stores">
                    <h4>Store Locator</h4>                  
                    <form class="navbar-form" role="search">
                      <div class="input-group">
                        <input type="text" class="form-control" placeholder="Enter your zip code" name="q">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit">Find</button>
                        </div>
                      </div>
                    </form>
                  </div>
                
                </div>
                </li>    

                 <li>
                <div class="st-acc-li-ca dropdown">
                <a class="icons-a"><img src="images/account.png"/></a>
                <a class="dropdown-toggle" type="button" id="account" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-user visible-xs-inline"></i> Account <span class="glyphicon glyphicon-chevron-down hidden-xs" aria-hidden="true"></span></a>
                  <div class="dropdown-menu find-store" aria-labelledby="account">
                  <button onclick="location.href='<?php echo $account; ?>'"  class="btn btn-danger btn-sm btn-block">My Account</button>
	        <button onclick="location.href='<?php global $url; echo $url->link('product/compare'); ?>'" class="btn btn-danger btn-sm btn-block">Product Compare</button>
                  <button onclick="location.href='<?php echo $order; ?>'" class="btn btn-danger btn-sm btn-block">Order History</button>
                  <button onclick="location.href='<?php echo $transaction; ?>'"  class="btn btn-danger btn-sm btn-block">Transactions</button>
                  <button onclick="location.href='<?php echo $download; ?>'"  class="btn btn-danger btn-sm btn-block">Downloads</button>
                </div> 
                </div>
                </li>

                
                <li>
                <div class="st-acc-li-ca dropdown">
                <a class="icons-a"><img src="images/mylist.png"/></a>
                <a href="<?php echo $wishlist ?>"><i class="fa fa-list visible-xs-inline"></i>  Wishlist </a>
                </div>
                </li>

             

                  <li class="carts">
                   <?php echo $cart; ?> 
                  </li>

                  

              
                 
              </ul>
            </div>
          </div>
          </div>  
        <!-- end of middle container -->
               






<!-- --------------------------------------------  Menu | wm-3 ----------------------------------------------------    -->
     

          <div class="menu container">
            <nav class="navbar navbar-default">
              <div class="">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    
                  </button>
                  <!-- <a class="navbar-brand hidden-sm hidden-md hidden-lg" href="#"><img src="images/logo.png" class="img-responsive" /></a> -->
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                

                 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                  <!--<li><a href="http://easasoft.com/stapleplus/index.php?route=allcategories">Office Supplies</a></li>-->

        <?php 
	
	foreach ($categories as $category) {  ?>

        <?php if ($category['children']) { 
          $newarray = array_slice($category['children'],0, 7);
          ?>
                    <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle megamenu-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $category['name']; ?><span class="sr-only">(current)</span></a>

                    <div class="dropdown-menu mega-dropdown-menu megamenu">
                        <div class="col-md-9 col-sm-12 col-xs-12">
                          <div class="row feature">
                            <div class="col-sm-12">
                              <h4>Featured Categories</h4>
                            </div>
                                   <?php foreach (array_chunk($newarray, ceil(count($newarray) )) as $children) { ?>
                                 <?php foreach ($children as $child) { ?>  
                                    <?php if($child['image']) { ?>                                      
                            <div class="col-sm-3">
                              <div class="subfeature">
                                <a href="<?php echo $child['href']; ?>"><img src="<?php echo $child['image'];?>" class="img-responsive"></a>
                                <p><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></p>
                              </div>
                            </div>
                            <?php } } } ?>
                        
                            <div class="col-sm-3">
                              <div class="subfeature">
                                <a href="<?php echo $category['href']; ?>"><img src="images/all-categories-arrow.png" class="img-responsive"></a>
                                <p><a href="<?php echo $category['href']; ?>"><?php echo $text_all; ?> <?php echo $category['name']; ?></a></p>
                              </div>
                            </div>
                            
                          </div>

                          
                         <div  class="row brands">
                            
                            <div class="fbrands col-xs-12">
                              <h4>Featured Brands</h4>
                            </div>

                            <div class="bimage">
                              <img src="images/brand1.jpg">
                            </div>
                            
                          </div>
                          
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12 dollardeals">
                          <h4>Dollar Deals</h4>
                          <p>simply dummy text of the printing and typesetting industry. </p>


   <div class="onefeatured">
        <a href=""><img src=""  class="img-responsive"></a>
      </div>



                        </div>
                        
                      </div>



                    </li>

                      <?php } else { ?>
                      <li>
                      <a href="<?php echo $category['href1']; ?>"><?php echo $category['name']; ?></a>
                      </li>
                     <?php } }?>
                    
                  </ul>
                </div><!-- /.navbar-collapse -->

              </div><!-- /.container-->
            </nav>
          </div>
          <!-- ............................ Wm-3 ........................................... -->


</div>

</header>





   <div class="container-fluid slider">
     <div class="row">
      <?php
           $url = 'easasoft.com'."$_SERVER[REQUEST_URI]";
            
            if( $url == 'easasoft.com/stapleplus/index.php?route=common/home')

            {
            ?>
       <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">

         
            <div class="item active">
              <img src="images/slider2.jpg" alt="...">
              <!-- <div class="carousel-caption">
                ...
              </div> -->
            </div>
            <div class="item">
              <img src="images/slider1.jpg" alt="...">
              
            </div>

            <div class="item">
              <img src="images/slider2.jpg" alt="...">
              
            </div>
        
            
          </div>

          <!-- Controls -->
          <!-- <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a> -->
        </div>
    <?php } ?>
        
     </div>
   </div>  
   <!-- ................end of slider and starting main part................... -->


<div class="main">
