<?php
// HTTP
define('HTTP_SERVER', 'localhost/stapleplus/');

// HTTPS
define('HTTPS_SERVER', 'localhost/stapleplus/');

// DIR
define('DIR_APPLICATION', '/opt/lampp/htdocs/stapleplus/catalog/');
define('DIR_SYSTEM', '/opt/lampp/htdocs/stapleplus/system/');
define('DIR_LANGUAGE', '/opt/lampp/htdocs/stapleplus/catalog/language/');
define('DIR_TEMPLATE', '/opt/lampp/htdocs/stapleplus/catalog/view/theme/');
define('DIR_CONFIG', '/opt/lampp/htdocs/stapleplus/system/config/');
define('DIR_IMAGE', '/opt/lampp/htdocs/stapleplus/image/');
define('DIR_CACHE', '/opt/lampp/htdocs/stapleplus/system/cache/');
define('DIR_DOWNLOAD', '/opt/lampp/htdocs/stapleplus/system/download/');
define('DIR_UPLOAD', '/opt/lampp/htdocs/stapleplus/system/upload/');
define('DIR_MODIFICATION', '/opt/lampp/htdocs/stapleplus/system/modification/');
define('DIR_LOGS', '/opt/lampp/htdocs/stapleplus/system/logs/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'stapleplus');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
