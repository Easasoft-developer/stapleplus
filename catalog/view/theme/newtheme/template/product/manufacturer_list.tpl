<?php echo $header; ?>

<div class="container mtb20">
<!-- breadcrumb -->
  <div class="row">      
          <ol class="breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ol>      
  </div>
</div>

  
<div class="container mb30">
  <div class="row">




  <div class="col-lg-9 col-md-9 col-xs-12 sidebar brandpage">
    <h4 class="sitecolor"> <span class="glyphicon glyphicon-star" aria-hidden="true"></span><?php echo $heading_title; ?></h4>
    <hr class="head-hr">



      <?php  if($categories) { ?>

      <h5><strong>Brand Index:</strong>
      <?php foreach ($categories as $category) { ?>
          &nbsp;&nbsp;&nbsp;<a href="index.php?route=product/manufacturer#<?php echo $category['name']; ?>"><?php echo $category['name']; ?></a>
          <?php } ?>
      </h5>
    

       <?php foreach ($categories as $category) { ?>
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title" id="<?php echo $category['name']; ?>"><?php echo $category['name']; ?></h3>
        </div>
        <div class="panel-body">
          <?php if ($category['manufacturer']) { ?>
          <?php foreach (array_chunk($category['manufacturer'], 4) as $manufacturers) { ?>
          <div class="row">      
          <?php foreach ($manufacturers as $manufacturer) { ?>
          <div class="col-md-3 col-sm-4 col-xs-6"><a href="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?></a></div>
          <?php }   ?>
          </div>
           <?php } } ?>
        </div>
      </div>
      <?php } ?>

  <?php } else { ?>

      <p><?php echo $text_empty; ?></p>

     <?php  } ?>

              
  </div>      




<?php if($allcategories) { ?>

      <div class="col-lg-3 col-md-3 col-xs-12">
          <div class="list-group sidebar">
              <div class="panel panel-default">
              
          <div class="panel-heading">
            <h4 class="panel-title">All Categories</h4>       
          </div>
      
          <div class="panel-body scrollpanel brandscroll">
            <div class="list-group">
            <?php foreach ($allcategories as $allcategory) { ?>
              <a href="<?php echo $allcategory['href']; ?>" class="list-group-item"><?php echo $allcategory['name']; ?></a>
                  <?php } ?>
            </div>
          </div>

        </div>
            </div>
      </div>
      <?php } ?>

  </div>
</div>


<?php echo $footer; ?>