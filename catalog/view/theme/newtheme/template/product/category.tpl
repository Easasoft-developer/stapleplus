
<?php echo $header; ?>
  
<div class="container mtb20">
<!-- breadcrumb -->
  <div class="row">      
    <ol class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ol>      
  </div>

</div>



  
<div class="container mb30">
<input type="hidden" id="categ" value="<?php echo $urlcate;?>">
  <div class="row">

     <!-- sidebar start -->
      <div class="col-lg-3 col-md-3 col-xs-12 sidebar">
      <div class="fffilter">
        <div class="panel panel-default">

<?php echo $column_left; ?>

          

          <div class="panel-heading">
            <h4 class="panel-title">Brands</h4>
          </div>

          <div class="panel-body scrollpanel allcatscroll">
            <div class="list-group">
		 <?php foreach ($manufacturer as $manufact) { ?>
              <li class="list-group-item">
              <label>
              <input name="brand[]" type="checkbox" value="<?php echo $manufact['id']; ?>">
              <?php echo $manufact['name']; ?>
              </label>
              </li>

              <?php  } ?>
	
            </div>
          </div>
</div>
        

        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">Dimensions (L x H x W)</h4>
          </div>
          <div class="panel-body scrollpanel allcatscroll">
            <div class="list-group alldim">
            
              <li class="list-group-item">
              <label><input name="dim[]" type="checkbox" value=""></label>
              </li>

            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">Price</h4>
          </div>
          <div class="panel-body allcatscroll">
            <div class="list-group fprices">
              <li class="list-group-item">
              <label><input name="fp[]" type="checkbox" value="0-500">AED 0 - AED 500</label>
              </li>
              <li class="list-group-item">
              <label><input name="fp[]" type="checkbox" value="500-1000">AED 500 - AED 1000</label>
              </li>
              <li class="list-group-item">
              <label><input name="fp[]" type="checkbox" value="1500-2000">AED 1500 - AED 2000</label>
              </li>
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">Availability</h4>
          </div>
          <div class="panel-body">
            <div class="list-group">
              <li class="list-group-item">
              <label><input name="sto[]" value="1" type="checkbox" value="">In Stock</label>
              </li>
              <li class="list-group-item">
              <label><input name="sto[]" value="0" type="checkbox" value="">Out of Stock</label>
              </li>
            </div>
          </div>
        </div>

        

</div>
</div>
    

      <!-- sidebar end -->

      <!-- categories display -->

      <div class="col-lg-9 col-md-9 col-xs-12">

      <hr class="head-hr">

  <div class="row head-view">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<h4 class="h20 sitecolor"><?php echo $heading_title; ?></h4>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="view-all"><a href="index.php?route=product/category&path=<?php echo $urlcate;?>">View All <span> Items >></span></a></div>
      </div>
  </div>

  <hr class="head-hr">



        <?php if (!ctype_space($description)) {  ?>

      <div class="row">
    <div class="col-xs-12 mb20">
    
    <article id="rmjs-1">

    <p class="cat-descript"><?php echo $description; ?>
    </p>

    </article>

    </div>


  </div>
    <?php } ?>  

  <hr class="head-hr">

  <div class="row">

      
      <div class="col-lg-7 col-md-8 col-sm-8 col-xs-12">
        <div class="filters">
          <span>Items 1 to 12</span>
          <select class="" id="input-limit" >
           <?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) { ?>
            <option name="limit" value="<?php echo $limits['value']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
             <?php } else { ?>
             <option name="limit" value="<?php echo $limits['value']; ?>"><?php echo $limits['text']; ?></option>
              <?php } ?>
            <?php } ?>
          </select>

          <span>By</span>

          <select class="" id="input-sort" >
              <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option name="sort" value="<?php echo $sorts['value']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
             <?php } else { ?>
            <option name="sort" value="<?php echo $sorts['value']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>



        </div>
      </div>
      <div class="col-lg-5 col-md-4 col-sm-4 col-xs-12">
        <div class="grid-list">
          <div class="btn-group" role="group" aria-label="...">            
          <button id="grid" class="btn btn-default" type="submit"><i class="glyphicon glyphicon-th-large"></i>Grid</button>
          <button id="list" class="btn btn-default" type="submit"><i class="glyphicon glyphicon-th-list"></i>List</button>
          </div>
        </div>
      </div>
  </div>

  <hr class="head-hr">



        <div id="products" class="row mt30 clearallcat">
  <?php if($products) { ?>
               <?php $bndval= 0; foreach ($products as $product) { ?>
    
               <div class="item col-lg-4 col-md-6 col-sm-6 col-xs-12 mb30">

            <div class="main-product-cat allproducts">



                <div class="product-thumb">
                  <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" class="img-responsive"></a>
                </div>
                <div class="allproduct-details">
                  
                  <div class="list-left">
                  <p><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></p>

                  <div class="review">
                     <?php if ($product['rating']) { ?>
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>
                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                  <?php } ?>
                  <?php } ?>
                <?php }  else {  ?><a href="<?php echo $product['href']; ?>#reviews">Write a review</a> <?php } ?> 
                    <!-- <img src="images/reviews.png"> -->
                    <!-- <span>654</span> <span>Reviews</span> -->
                  </div>



                <div class="price">
                  <?php if ($product['price']) { ?>
                     <?php if (!$product['special']) { ?>
                  <?php echo $product['price']; ?>
                  <?php } else { ?>
                   <span><?php echo $product['special']; ?></span>
                    <?php } ?>
                  <?php } ?>
                  </div>


                  </div>


                  <div class="list-right">

    
                  <div class="qty-btn button-group">
                    <span>Quantity</span>
                    <input class="input-quantity" type="text" value="" id="product_qty1<?php echo $bndval; ?>">
                    <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>','<?php echo $bndval; ?>');" class="btn btn-default">ADD TO CART</button>
                  </div>

                  <div class="button-group">
                  <p><a style="cursor:pointer" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><img class="mr5" src="images/wishlist-icon.png">Add to Wish List</a></p>
                  <p><a style="cursor:pointer" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><img class="mr5" src="images/add-compare-icon.png">Add to Compare</a></p>
                  <p><img class="mr5" src="images/tick.png"><?php if( $product['quantity'] > 0 ) { echo 'In Stock'; } else { echo 'Out of stock'; }  ?></p>
                  </div>


                  </div>
                  
                </div>



            </div>


          </div>



        <?php $bndval++; } ?>
					<?php } else { echo "<div class='col-xs-12'><div class='alert alert-info' role='alert' >Sorry. There is <strong>no product available</strong>. Come back later !!!</div></div>"; } ?>
        </div>


      </div>

      <!-- categories end -->

     

  </div>


  <div class="row">
      <div class="col-xs-12">
        <nav>
         <?php echo $pagination;?>
        </nav>
      </div>

      </div>

      
    <!--   <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
        <div><a href="<?php echo $continue; ?>"><?php echo $button_continue; ?></a></div>
      <?php } ?>
 -->

</div>

<!-- end of product container -->
<script type="text/javascript">

$(document).ready(function () {

$('input[name="sto[]"]').on('change', function() {
$('input[name="sto[]"]').not(this).prop('checked', false); 
});

$('input[name="fp[]"]').on('change', function() {
$('input[name="fp[]"]').not(this).prop('checked', false); 
});

  
  $(".input-quantity").keypress(function (e) {
  
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               return false;
    }
   });
});
</script>





<script type="text/javascript">
   
  $.ajax({
type: 'post',
url: 'index.php?route=product/ajaxcall',
dataType: 'json',
success: function(json) {
vv = json.alldim;
var khtml = '';
$.each(vv,function(k,kfil) {

khtml += '<li class="list-group-item"><input value="'+kfil.length+' '+kfil.height+' '+kfil.width+'" name="dim[]" type="checkbox">'

+Math.round(kfil.length)+'mm'+'&nbsp;x&nbsp;'+Math.round(kfil.height)+'mm'+'&nbsp;x&nbsp;'+Math.round(kfil.width)+'mm'+'</li>';

});
$('.alldim').html(khtml);
}

});

</script>






<script type="text/javascript">
$(document).ready(function() {

var pricearr= '';
var branadarr = new Array();
var stockarr = new Array();
var dimenarr = new Array();
var cate=$('#categ').val();
var limit = $('option[name="limit"]').val();
var sort = $('option[name="sort"]').val();





 $('.list-group-item').click(function(){
 branadarr = new Array();
 stockarr = new Array();
 dimenarr = new Array();
$.each($("input[name='brand[]']:checked"), function() {
  branadarr.push($(this).val());
});
  pricearr=($("input[name='fp[]']:checked").val()!=undefined)?$("input[name='fp[]']:checked").val():'';
  stockarr.push(($("input[name='sto[]']:checked").val()!=',')?$("input[name='sto[]']:checked").val():0);
  dimenarr.push(($('.alldim').find("input[name='dim[]']:checked").val()!=',')?$("input[name='dim[]']:checked").val():0);
  ajaxFilter(branadarr,pricearr,stockarr,dimenarr,cate,limit,sort);

});
 
$('select').change(function() {
 limit = $('option[name="limit"]:selected').val();
 sort = $('option[name="sort"]:selected').val();
 console.log(limit+'-'+sort);
 ajaxFilter(branadarr,pricearr,stockarr,dimenarr,cate,limit,sort);
});
 function ajaxFilter(branadarr,pricearr,stockarr,dimenarr,cate,limit,sort){



$.ajax({
type: 'post',
url: 'index.php?route=product/ajaxcall',
data: 'brandarr='+branadarr+'&pricearr='+pricearr+'&stockarr='+stockarr+'&dimenarr='+dimenarr+'&cate='+cate+'&limit='+limit+'&sort='+sort,
dataType: 'json',
 beforeSend: function() {
	     $('#products').html('<img class="center-block" src="catalog/view/theme/newtheme/image/loader_new.gif">');
    
},
success: function(json) {


   ff = json.success;
//console.log(ff);
var fhtml =''; 
var btnval = 0;
if(ff.length != 0) {
$.each(ff,function(i,pfil){

  var stockcheck = '';
  var price = '';
var rating = '';
// alert(pfil.discount);
//console.log(pfil.rating);
  if(pfil.discount == null ) { price =  'AED '+parseFloat(pfil.origprice).toFixed(2);  } else { price = 'AED'+parseFloat(pfil.discount).toFixed(2); }
  if(pfil.quantity > 0){ stockcheck = 'In Stock';} else { stockcheck = 'Out of Stock'; }


 if(pfil.rating == null ) {
 rating = '<a href="index.php?route=product/product&product_id='+pfil.product_id+'#reviews">Write a review</a>'; 
 } else {

 for(var i = 1; i <= 5; i++) {

  if( pfil.rating < i ) { 

rating += '<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>'; 
} else { 
rating += '<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>'; 
}    

 }
 }




  fhtml += '<div class="item col-lg-4 col-md-6 col-sm-6 col-xs-12 mb30"><div class="main-product-cat allproducts"> <div class="product-thumb">'+
                  '<a href="index.php?route=product/product&product_id='+pfil.product_id+'"><img src="'+pfil.image+'"class="img-responsive">'+'</a>'+'</div>'+

                '<div class="allproduct-details"><div class="list-left">'+
                  '<p><a href="index.php?route=product/product&product_id='+pfil.product_id+'">'+pfil.name+'</a></p>'+rating
+'<div class="price"><p><span>'+price+'</span></p></div></div>'+
    
                  '<div class="list-right"><div class="qty-btn button-group">'+
                    '<span>Quantity</span>&nbsp;'+
                    '<input class="input-quantity" type="text" value="" id="product_qty1'+btnval+'">&nbsp;'+
                    '<button type="button" onclick="cart.add('+pfil.product_id+','+i+');" class="btn btn-default">ADD TO CART</button>'+
                  '</div>'+
                  '<div class="button-group">'+
                  '<p><a style="cursor:pointer" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('+pfil.product_id+');"><img class="mr5" src="images/wishlist-icon.png">Add to Wish List</a></p>'+
                  '<p><a style="cursor:pointer" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('+pfil.product_id+');"><img class="mr5" src="images/add-compare-icon.png">Add to Compare</a></p>'+
                  '<p><img class="mr5" src="images/tick.png">'+stockcheck+'</p>'+
                  '</div></div>'+
                  
                '</div></div></div>';
                btnval++;

});
}
else
{
fhtml += "<div class='col-xs-12'><div class='alert alert-info' role='alert' >Sorry. There is <strong>no product available</strong>. Come back later !!!</div></div>";
}

$('#products').html(fhtml);



 }

});



}

});

</script>






<?php echo $footer; ?>


