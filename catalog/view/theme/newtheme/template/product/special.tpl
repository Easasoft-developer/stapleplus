<?php echo $header; ?>
  
<div class="container mt10">
<!-- breadcrumb -->
  <div class="row">      
          <ol class="breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>   
            <?php } ?>
          </ol>      
  </div>
</div>

    <div id="content" class="container mb30 searchpage">

    <?php echo $content_top; ?>
     

      <h4><?php echo $heading_title; ?></h4>


      <?php if ($products) { ?>
  









    <hr class="head-hr">


  <div class="row searchfilter">



        <div class="col-md-1 col-xs-12">
          <h5 class="control-label" for="input-sort"><?php echo $text_sort; ?></h5>
        </div>


        <div class="col-lg-2 col-md-3 col-xs-12">
          <select id="input-sort" class="form-control" onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>


        <div class="col-md-1 col-xs-12">
          <h5 class="control-label" for="input-limit"><?php echo $text_limit; ?></h5>
        </div>

        <div class="col-lg-2 col-md-3 col-xs-12">
          <select id="input-limit" class="form-control" onchange="location = this.value;">
            <?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) { ?>
            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
  </div>

<hr class="head-hr hidden-xs hidden-sm">



     <div id="special-search" class="row mt30">
      <?php $bndval= 1; ?>
           <?php foreach ($products as $product) {  ?>
          <div class="item col-md-4 col-sm-6 col-xs-12 mb30">
            <div class="main-product-cat allproducts">
                <div>
                  <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" class="img-responsive"></a>
                </div>
                <div class="allproduct-details">
                  <p><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></p>



                     
              <div class="rating">
<?php if ($product['rating']) { ?>
                <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($product['rating'] < $i) { ?>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } else { ?>
                <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } ?>
                <?php } ?>
 <?php }   else { ?> <a href="<?php echo $product['href'].'#reviews'; ?>">Write a review</a>  <?php }   ?>
              </div>
             




              <?php if ($product['price']) { ?>
                  <div class="price">
                  <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>
                <?php } else { ?>
                    <p><span><?php echo $product['special']; ?></span> <del ><?php echo $product['price']; ?> </del></p>
                    <?php } ?>
                  </div>
                  <?php } ?>


                  <div class="qty-btn">
                    <span>Quantity</span>
                    <input type="text" value"" name="product_qty" id="product_qty1<?php echo $bndval; ?>">
                    <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>','<?php echo $bndval; ?>');" class="btn btn-default">ADD TO CART</button>
                  </div>

                  <p><a onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><img  class="mr5" src="images/wishlist-icon.png">Add to Wish List</a></p>
                  <p><a onclick="compare.add('<?php echo $product['product_id']; ?>');"><img  class="mr5" src="images/add-compare-icon.png">Add to Compare</a></p>
                  <p><img class="mr5" src="images/tick.png"><?php  if( $product['quantity'] > 0 ) { echo 'In Stock'; } else { echo 'Out of stock'; }  ?></p>
                  
                </div>
            </div>
          </div> 


       <?php $bndval++; }  ?>
        </div>











      <div class="row">
        <div class="col-sm-6 text-left"><?php // echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $pagination; ?></div>
      </div>

      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>


      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>



      <?php echo $content_bottom; ?>


    




    <?php echo $column_right; ?>
</div>









<script type="text/javascript">

$(document).ready(function () {
  
  $("input[name='product_qty']").keypress(function (e) {
  
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               return false;
    }
   });
});
</script>
 
<?php echo $footer; ?>

