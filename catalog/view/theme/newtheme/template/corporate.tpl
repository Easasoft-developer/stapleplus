

<?php  echo $header;?>
<div class="container mt20">
  <div class="row mb30">
    <div class="col-md-8">
      <h4 class="sitecolor h20">Corporate Customer & Govt. Customers</h4>
      <p>Thanks for your interest in our 30 Day Business Account.The team at Stapleplus is here to help you grow your business. That's why we’ve designed the 30 Day Business Account to give you an extra advantage.</p>
       <strong>30 days and many advantages:</strong>
              <ul>
                <li>Extended Credit Terms - 30 days to pay for your orders eases monthly cash flow</li>
                <li>Simple Accounting - Just one consolidated monthly bill and a clear list of expenditures</li>
                <li>Flexible Shopping Options - Shop online, in store, over the phone or by fax</li>
              </ul>
      <div class="row">
        <div class="col-sm-6 col-md-6">
          <div class="thumbnail">
            <img src="images/big.jpg" alt="...">
            <div class="caption">
              <?php if($cinfo[0]['information_id'] == 14) { ?>
              <h4 class="corp-head"><a href="index.php?route=information/information&information_id=<?php echo $cinfo[0]['information_id'];?>"><?php echo $cinfo[0]['title']; ?></a></h4>
              <?php echo html_entity_decode(substr($cinfo[0]['description'],0,150)).'...'; ?>
              <h4>
                <a href="index.php?route=information/information&information_id=<?php echo $cinfo[0]['information_id'];?>" class="btn btn-default" role="button">Read More</a>
              </h4>
              <?php   } ?>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-6">
          <div class="thumbnail">
            <img src="images/big.jpg" alt="...">
            <div class="caption">
              <?php if($cinfo[2]['information_id'] == 17) { ?>
              <h4 class="corp-head"><a href="index.php?route=information/information&information_id=<?php echo $cinfo[2]['information_id'];?>"><?php echo $cinfo[2]['title']; ?></a></h4>
              <p><?php echo html_entity_decode(substr($cinfo[2]['description'],0,150)).'...'; ?></p>
              <h4>
                <a href="index.php?route=information/information&information_id=<?php echo $cinfo[2]['information_id'];?>" class="btn btn-default" role="button">Read More</a>
              </h4>
              <?php } ?>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-md-12">
          <div class="thumbnail">
            <img src="images/big.jpg" alt="...">
            <div class="caption">
              <?php if($cinfo[1]['information_id'] == 15) { ?>
              <h4><a href="index.php?route=information/information&information_id=<?php echo $cinfo[1]['information_id'];?>"><?php echo $cinfo[1]['title']; ?></a></h4>
              <p><?php echo html_entity_decode(substr($cinfo[1]['description'],0,150)).'...'; ?></p>
              <h4>
                <a href="index.php?route=information/information&information_id=<?php echo $cinfo[1]['information_id'];?>" class="btn btn-default" role="button">Read More</a>
              </h4>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
      </div>
      


      <div class="col-md-4">
        <div class="row">
          <div class="col-xs-12">
            <div class="well">
              <p>To apply for a 30 Day Credit Account please complete the form below, you will be contacted by one of our sales associates within 24 hours and informing you of further procedures.
                Please note that credit terms are only offered to business customers who pass our credit check.
              </p>
             
              <form method="post">
                <div class="form-group">
                  <input type="text" class="form-control" aria-describedby="name-format" name="fname" placeholder="First Name" aria-required=”true”  required>
                  <!--                <span id="name-format" class="help">Format: Firstname</span>-->
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="lname" placeholder="Last Name">
                </div>
                <div class="form-group">
                  <input type="tel" class="form-control" name="cno" placeholder="Contact Number" aria-required=”true” required>
                </div>
                <div class="form-group">
                  <input type="email"  name="email" class="form-control" placeholder="example@domain.com" aria-required=”true” required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="BName" placeholder="Business Name" aria-required=”true” required>
                </div>
                <div class="form-group">
                  <input type="tel" class="form-control" name="zip" placeholder="Zip Code" aria-required=”true” required>
                </div>
                <div class="form-group">
                  <select name="nofemp" class="form-control" required>
                    <option>Number of Employees</option>
                    <option>1 - 5</option>
                    <option>5 - 20</option>
                    <option>20 - 50</option>
                    <option>50 - 100</option>
                    <option>100 - 250</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Message:</label>
                  <textarea name="msg" class="form-control" rows="3"></textarea>
                </div>
                <input class="btn btn-default" type="submit" name="submit" value="SUBMIT">
              </form>
              <?php




                if (isset($_REQUEST['email']) && $_REQUEST['email'] != '' )  {
                
                $admin_email = "menonjats@gmail.com";
                $email = $_REQUEST['email'];
                $subject = "Corporate Customer & Govt. Customers";
                
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= "From: $email" . "\r\n";
                
                
                // $comment = " First Name: ".$_REQUEST['fname']."\r\n Last Name: ".$_REQUEST['lname']."\r\n Contact No: ".$_REQUEST['cno']."\r\n Bussiness no: ".$_REQUEST['BName']."\r\n Zip code: ".$_REQUEST['zip']."\r\n Messsage: ".$_REQUEST['msg']."\r\n No of Employee: ".$_REQUEST['nofemp'];
                
                $comment = '<html><body>';
                $comment .= '<table rules="all" style="border:1px solid #666;" cellpadding="10">';
                $comment .= "<tr><td style='background: #eee;'><strong>First Name</strong> </td><td>" .$_REQUEST['fname']. "</td></tr>";
                $comment .= "<tr><td style='background: #eee;'><strong>Last Name</strong> </td><td>" .$_REQUEST['lname']. "</td></tr>";
                $comment .= "<tr><td style='background: #eee;'><strong>Contact No</strong> </td><td>" .$_REQUEST['cno']. "</td></tr>";
                $comment .= "<tr><td style='background: #eee;'><strong>Business no</strong> </td><td>" .$_REQUEST['BName']. "</td></tr>";
                $comment .= "<tr><td style='background: #eee;'><strong>Zip code</strong> </td><td>" .$_REQUEST['zip']. "</td></tr>";
                $comment .= "<tr><td style='background: #eee;'><strong>Messsage</strong> </td><td>" .$_REQUEST['msg']. "</td></tr>";
                $comment .= "<tr><td style='background: #eee;'><strong>No of Employees</strong> </td><td>" .$_REQUEST['nofemp']. "</td></tr>";
                $comment .= "</table>";

                $comment .= "</body></html>";

                
                
                mail($admin_email, $subject, $comment, $headers);
                
                echo "<p class='corporate'><i class='fa fa-thumbs-o-up'></i> Thank you for contacting us!</p>";
                }  
                
                /* else  {
                echo "<p>Sorry! Error Occurred..</p>";
                } */
                ?>
            </div>
          </div>
        </div>
      </div>    
  
</div>
</div>
<script>
  $(document).ready(function(){
  
   $("input[name='cno'], input[name='zip']").keypress(function (e) {
    
  if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                 return false;
      }
     });
  
  
  
  $("input[name='fname'],input[name='lname']").keypress(function (e) {
      if (e.which != 8 && e.which != 0 && (e.which < 97 /* a */ || e.which > 122 /* z */) ) {
        return false;
      }
  });
  
  
  $("input[name='cno'], input[name='zip']").on('paste', function (event) {
    if (event.originalEvent.clipboardData.getData('Text').match(/[^\d]/)) {
      event.preventDefault();
    }
  });
  
  $("input[name='fname'], input[name='lname']").on('paste', function (event) {
    if (!event.originalEvent.clipboardData.getData('Text').match(/[^\d]/)) {
      event.preventDefault();
    }
  });
  
  
  });
  
  
</script>
<?php echo $footer; ?>

