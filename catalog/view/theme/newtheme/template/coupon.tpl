<?php echo $header; ?>

<div class="container">
	<div class="row only-coupons">
			<div class="col-sm-12">
				<h4 class="sitecolor">Coupons</h4>
			</div>	


			<?php if(!empty($coupon_info)) { ?>

			<?php foreach ($coupon_info as $coupon) { ?>	

			<div class="col-sm-3">
				<div class="panel panel-default">
			 		<div class="panel-heading"><a href="index.php?route=product/product&product_id=<?php echo $coupon['product_id']; ?>"><img src="<?php echo $coupon['image']; ?>" />&nbsp;<?php echo $coupon['pname']; ?></a></div>
			 		<div class="panel-body">
			 			<p><i class="fa fa-scissors"></i>&nbsp;Coupon code:&nbsp;<?php echo $coupon['code']; ?> </p>
						<p><i class="fa fa-tag">&nbsp;</i>Item code:&nbsp;<?php echo $coupon['product_id']; ?></p>
			 			<p><i class="fa fa-hourglass-start"></i>&nbsp;Started on:&nbsp;<?php echo $coupon['date_start']; ?></p>
			 			<p><i class="fa fa-hourglass-end"></i>&nbsp;Expires on:&nbsp;<?php echo $coupon['date_end']; ?></p>
			 			<p><i class="fa fa-barcode"></i>&nbsp;Discount:&nbsp;<?php echo round($coupon['discount'],2)." AED"; ?></p>
			 			<p><i class="fa fa-sort-amount-desc"></i>&nbsp;Minimum purchase:&nbsp;<?php if($coupon['total'] > 0) { echo round($coupon['total'],2)." AED"; } else { echo "No"; } ?></p>
			 		</div>
		 		</div>
			</div>

			<?php } } else { ?>
					<div class="col-sm-12">
						<h4 class="animated shake"><i class="fa fa-clock-o"></i> Sorry folks ! Come back later. </h4>
					</div>
			<?php } ?>
			
	</div>
</div>


<?php echo $footer;?>
