<div class="container mb30">
      <div class="row">

        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
          <div class="member-discount">
            <a href="index.php?route=coupon"><img src="images/discount.png"></a>
              <h4><a href="index.php?route=coupon">coupon codes</a></h4>
              <p>simply dummy text of the printing and typesetting industry.</p>
          </div>
        </div>

        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
          <div class="freeshipping">
            <a href="index.php?route=information/information&information_id=12"><img src="images/shipping_icon.png"></a>
              <h4><a href="index.php?route=information/information&information_id=12">Free Delivery</a></h4>
              <p>simply dummy text of the printing and typesetting industry.</p>
          </div>
        </div>

        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
          <div class="fastdelivery">
            <a href="index.php?route=information/information&information_id=10"><img src="images/delivery.png" height="52"></a>
              <h4><a href="index.php?route=information/information&information_id=10">Return and Exchange</a></h4>
              <p>simply dummy text of the printing and typesetting industry.</p>
          </div>
        </div>

      </div>      
    </div>



<div class="container mb30">
      <div class="row">

<div class="col-xs-12">
          <h3 class="text-center">Top Brands</h3>
        </div>
<div class="col-xs-12">
         
<div id="carousel<?php echo $module; ?>" class="owl-carousel">
  <?php foreach ($banners as $banner) { ?>
  <div class="item text-center">
    <?php if ($banner['link']) { ?>
    <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
    <?php } else { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
    <?php } ?>
  </div>
  <?php } ?>
</div>

  </div>
   </div>      
    </div>

<script type="text/javascript"><!--
$('#carousel<?php echo $module; ?>').owlCarousel({
	items: 5,
	autoPlay: 3000,
	navigation: true,
	pagination: true
});
--></script>
