
  <!-- ----------------------------------  HOT DEALS ---------------------------------------------- -->



<div class="container mb30">

      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a><?php echo $heading_title; ?></a></li>
        
      </ul>

  <!-- Tab panes -->  
          
        <div class="row">
        <div class="col-xs-12">
         <div class="deals">
        <div class="autoplay">

          <?php foreach ($products as $product) { ?>
        <div class="">
          <div class="hotdeals">
            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" class="img-responsive"></a>
               <?php if ($product['rating']) { ?>
        <div class="review">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($product['rating'] < $i) { ?>
          <span><img src="images/0star.png"></span>
          <?php } else { ?>
          <span><img src="images/1star.png"></span>
          <?php } ?>
          <?php } ?>
        </div>
        <?php } ?>
              
            <p><?php echo $product['name']; ?></p>
            <div><a class="shopnow" href="<?php echo $product['href']; ?>">Shop Now<span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span></a></div>
              <?php if ($product['price']) { ?>
        <div class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span><?php echo $product['special']; ?></span> <del><?php echo $product['price']; ?></del>
          <?php } ?>
          
        </div>
        <?php } ?>
          </div>
        </div>


       <!--  <div class="">
          <div class="hotdeals">
            <a><img src="images/hotdeals1.png" class="img-responsive"></a>
            <div class="review">
              <img src="images/reviews.png" class="img-responsive">
              <span>(654)</span>
            </div>
            <p>Cameras & Photo</p>
            <div><a class="shopnow" href="#">Shop Now<span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span></a></div>
            <div class="price">
            <span>AED 499.99 </span>
            <del>AED 599.99</del>
            </div>
          </div>
        </div>  -->

        <?php } ?>

        </div>
        </div>
        </div>
        </div> 
        

</div>

<!-- --------------------------------------------------  HOT DEALS ENDS ------------------------------------------- -->
