    <!-- Top Trending Products -->

    <div class="container mb30" >

      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a><?php echo $heading_title; ?></a></li>
        
      </ul>

  <!-- Tab panes -->  
        
        <div class="row">
        <div class="col-xs-12">
         <div class="deals">
        <div class="trends">
        <?php foreach ($products as $product) { ?>
          <div>
          <div class="hotdeals toptrends">
              <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" class="img-responsive"></a>
              

              <div class="review">
              <?php if ($product['rating']) { ?>        
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($product['rating'] < $i) { ?>
          <!--<span class="fa fa-stack"><i class="fa fa-star-o fa fa-star-o fa-stack-2x"></i></span>-->
	 <span><img src="images/1star.png"></span>
          <?php } else { ?>
          <span><img src="images/0star.png"></span>
          <?php } ?>
          <?php } ?>
        <?php } ?>
        </div>

              <p><?php echo $product['name']; ?></p>   
 <?php if ($product['price']) { ?>
        <div class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span><?php echo $product['special']; ?></span> <del><?php echo $product['price']; ?></del>
          <?php } ?>
        </div>
        <?php } ?>

            </div>
          </div>


          <?php } ?>

        </div> 
        </div>
        </div>
        </div> 

</div>
