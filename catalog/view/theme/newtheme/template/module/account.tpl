          <div class="list-group account-sidebar">
              <a href="<?php echo $account; ?>" class="list-group-item active">
              <span class="glyphicon glyphicon-user" aria-hidden="true"></span> My Account</a>

              <a href="<?php echo $edit; ?>" class="list-group-item">
              <span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit Account</a>
              
              <a href="<?php echo $password; ?>" class="list-group-item">
              <span  href="<?php echo $password; ?>" class="glyphicon glyphicon-lock" aria-hidden="true"></span> Password</a>
              
              <a href="<?php echo $address; ?>" class="list-group-item">
              <span class="glyphicon glyphicon-book" aria-hidden="true"></span> Address Books</a>
              
              <a href="<?php echo $wishlist; ?>" class="list-group-item">
              <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Wishlist</a>
              
              <a href="<?php echo $order; ?>" class="list-group-item">
              <span class="glyphicon glyphicon-duplicate" aria-hidden="true"></span> Order History</a>
              
              <a href="<?php echo $return; ?>" class="list-group-item">
              <span class="glyphicon glyphicon-transfer" aria-hidden="true"></span> Returns</a>
              
              <a href="<?php echo $newsletter; ?>" class="list-group-item">
              <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Newsletter</a>
              
              <?php if (!$logged) { ?>
              <a href="<?php echo $login; ?>" class="list-group-item">
              <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span><?php echo $text_login; ?></a>
              <?php } else if ($logged) { ?>
              <a href="<?php echo $logout; ?>" class="list-group-item">
              <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span><?php echo $text_logout; ?></a>
              <?php } ?>
            </div>

