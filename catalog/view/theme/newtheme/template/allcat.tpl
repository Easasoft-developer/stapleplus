<?php echo $header; ?>

<div class="container mtb20">

<!-- breadcrumb -->
  <div class="row">   
          <ol class="breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ol>      
  </div>
</div>
  
<div class="container mb30">
  <div class="row">

  <!-- sidebar start -->
      <div class="col-lg-3 col-md-3 col-xs-12 sidebar">

        <?php if($categories) { ?>
    <div class="panel panel-default">

        
          <div class="panel-heading">
            <h3 class="panel-title">All Categories</h3>
          </div>

          <div class="panel-body scrollpanel">
            <div class="list-group">
             
          <?php foreach ($categories as $category) { ?>
              <a href="<?php if(empty($category['children'])) { echo $category['href1']; } else {  echo $category['href']; } ?>" class="list-group-item"><?php echo $category['name']; ?></a>
                <?php } ?>
            </div>
          </div>
        
        </div>

<?php  } ?>


        
        <?php if($manufacturer) { ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Shop by Brand</h3>
          </div>
          <div class="panel-body scrollpanel">
            <div class="list-group">
            <?php foreach ($manufacturer as $manufact) { ?>
              <a href="<?php echo $manufact['href']; ?>" class="list-group-item"><?php echo $manufact['name']; ?></a>
              <?php  } ?>
            </div>
          </div>
        </div>    
        <?php } ?>


      </div>

      <!-- sidebar end -->

      <!-- categories display -->

      <div class="col-lg-9 col-md-9 col-xs-12">

      <hr class="head-hr">

  <div class="row head-view">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h4 class="h20"><?php echo $heading_title; ?> </h4>
     </div>
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
  <!--      <div class="view-all"><a href="<?php echo $category['href1']; ?>">View All <span>Products >></span></a></div> -->
      </div>
  </div>

  <hr class="head-hr">
<!-- <div class="mb30"></div> -->
      <div class="row">
    <div class="col-xs-12 mb20">
    <article id="rmjs-1">
    <p class="cat-descript more"><?php echo $description; ?></p>
   </article>
    </div>
  </div>
        <div id="clearallcat" class="row">
          
        

        
 <?php foreach ($categories as $category) { ?>
          <div class="col-md-4 col-sm-6 col-xs-12 mb30">
            <div class="main-product-cat">
                <div>
                  <a href="<?php if(empty($category['children'])) { echo $category['href1']; } else {  echo $category['href']; } ?>"><img src="<?php echo $category['thumb']; ?>" class="img-responsive"></a>
                </div>

                <div class="mpc-details">
                  <h4><a href="<?php if(empty($category['children'])) { echo $category['href1']; } else {  echo $category['href']; } ?>"><?php echo $category['name']; ?></a></h4>
                 
                  

                  <ul class="list-group">
 <?php 
                   if($category['children']) {
                   	$i = 0;
foreach($category['children'] as $child ) {
	if(++$i > 2) break;

   ?>
                    <li class="list-group-item"><a href="<?php echo $child['href']; ?>"><i class="fa fa-location-arrow"></i><?php echo $child['name']; ?></a></li>
                     <?php } }  ?> 
                  </ul>
                
                </div>
<?php if(!empty($category['children'])) { ?>
                <div class="all-cat-btn">
	
                  <a href="<?php if(empty($category['children'])) { echo $category['href1']; } else {  echo $category['href']; } ?>">All Categories <span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span></a>
 </div>
<?php } else {?>
 
<?php } ?>
              

            </div>
          </div>
          <?php } ?>

        </div>


      </div>

      <!-- categories end -->
      

  </div>

  <div class="row">
      <div class="col-xs-12">
        <nav>
          <!-- <?php echo $pagination; ?> -->
        </nav>
      </div>

      </div>
</div>



   <script type="text/javascript">
    $('article').readmore({
      collapsedHeight: 100,
        afterToggle: function(trigger, element, expanded) {
          if(! expanded) { // The "Close" link was clicked
            $('html, body').animate( { scrollTop: element.offset().top }, {duration: 200 } );
          }
        }
      });
    </script>


  
<?php echo $footer; ?>
