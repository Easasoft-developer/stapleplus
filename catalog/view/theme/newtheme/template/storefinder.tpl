<?php echo $header; ?>


<div class="container mt20">
<!-- breadcrumb -->
  <div class="row">      
          <ol class="breadcrumb">
            <li><a href="#">Home</a></li>           
            <li class="active">Search</li>
          </ol>      
  </div>
</div>

  
<div class="container">
  
  <div class="row">
    
<div class="clearfix"></div>

      <div class="col-md-9 col-xs-12">
      <div class="row">
    <div class="col-sm-6 col-xs-12">
      <h4 class="sitecolor">Store Locator</h4>
      <p>Find the store nearby your location</p>      
    </div>
    <div class="col-sm-6 col-xs-12">
        <div class="input-group">
            <div class="input-group-addon" style="border-radius:0x;"><i class="fa fa-map-marker"></i></div>
            <input class="form-control" value="<?php echo $finder; ?>" type="text" name="s_search" placeholder="Enter your zip code">
            <span class="input-group-btn">
              <button id="map_search" type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
            </span>
          </div>
          <a class="sitecolor mt10" style="margin:10px 0;display:inline-block;" href="index.php?route=storefinder"><i class="fa fa-arrow-circle-right"></i>&nbsp;View All Stapleplus Locations</a>  
    </div>
    </div>
        <div id="map" style="height: 500px;"></div>
      </div>

      <div class="col-md-3 col-xs-12">      
       
        <!-- <div class="input-group">
          <input class="form-control" value="<?php echo $finder; ?>" type="text" name="s_search" placeholder="Enter your zip code"> 
          <button id="map_search" class="btn btn-default">Find</button>
          <a class="" href="index.php?route=orderbyitem">View All Office Depot Locations</a>
        </div> -->

       <!--  <div class="input-group">
            <div class="input-group-addon" style="border-radius:0x;"><i class="fa fa-map-marker"></i></div>
            <input class="form-control" value="<?php echo $finder; ?>" type="text" name="s_search" placeholder="Enter your zip code">
            <span class="input-group-btn">
              <button id="map_search" type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
            </span>
          </div> 
          <a class="sitecolor mt10" style="margin:10px 0;display:inline-block;" href="index.php?route=storefinder"><i class="fa fa-arrow-circle-right"></i>&nbsp;View All Office Depot Locations</a>-->
        <h4>Browse Locations</h4>
        <div id="map_data" class="storelocator"></div>
      </div>
  </div>
</div>




<!-- <div id="map" style="width:50%;height:502px;float:left; margin-left:100px; margin-right:20px; margin-bottom:100px; "> </div> -->
<!-- <br>
<div class="input-group">
<input  class="form-control" value="<?php echo $finder; ?>" type="text" name="s_search" placeholder="Enter your zip code"> <button id="map_search" class="btn btn-default">Find</button>
<a class="" href="index.php?route=orderbyitem">View All Office Depot Locations</a>
</div> -->
<!-- <div id="map_data"></div> -->




<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>

<script type="text/javascript">

var locations = new Array();
    function googleMap() {
      
//        getAgentlatlong(base_url, cont);
//         var locations = [
//      ['Bondi Beach', -33.890542, 151.274856, 4],
//      ['Coogee Beach', -33.923036, 151.259052, 5],
//      ['Cronulla Beach', -34.028249, 151.157507, 3],
//      ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
//      ['Maroubra Beach', -33.950198, 151.259302, 1]
//    ];

       

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: new google.maps.LatLng(24.4667, 54.3667),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][2], locations[i][3]),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                  var addresses='<p style="color:red;">'+locations[i][0]+','+locations[i][1]+'</p>'
                    infowindow.setContent(addresses);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }








</script>


<script type="text/javascript">
store_search();
 var locations = new Array();
function store_search(){

var branadarr = $("input[name='s_search']").val();
  $.ajax({
type: "post",
url: "index.php?route=storefinder/myFun",
dataType: "json",
data: 'brandarr='+branadarr,
success: function(json) {



var fb = json.getval;
$.each(fb,function (i, res) {
                    locations[i] = [res.store_name, res.store_address , res.latitude, res.longitude];
                });


googleMap();
var html = '';
if(fb.length != 0) {
$.each(fb,function (i,mdata){ 

html += '<address><strong>'+ mdata.store_name+'</strong><br>'+ mdata.store_address+'<br>'+ mdata.zipcode+'<br>'+ mdata.city+'</address>';

// html += '<div class="panel panel-info">          
//           <div class="panel-heading"><strong>'+ mdata.store_name+'</strong></div>  
          
//           <ul class="list-group"><li class="list-group-item">'+ mdata.store_address+'</li><li class="list-group-item">'+ mdata.zipcode+'</li><li class="list-group-item">'+ mdata.city+'</ul></div></div>';

});
}

else { html += '<div class="text-danger"><strong><i class="fa fa-exclamation-triangle"></i> No store found. Please try again!</strong><i></div>'; } 
$('#map_data').html(html);

}
});
}


$(document).on('click','#map_search',function() {
   locations=new Array('');
store_search();
 googleMap();
});
</script>    

<?php echo $footer;?>
