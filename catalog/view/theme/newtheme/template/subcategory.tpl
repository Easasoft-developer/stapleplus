
<?php echo $header; ?>

<div class="container mtb20">
<!-- breadcrumb -->
  <div class="row">      
          <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Office Supplies</a></li>
            <li class="active">Filling Storage</li>
          </ol>      
  </div>

<hr class="head-hr">

  <div class="row head-view">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h4 class="h20">Filling Storage</h4>
      </div>
  </div>

  <hr class="head-hr">

  

</div>

  
<div class="container mb30">
  <div class="row">

  <!-- sidebar start -->
      <div class="col-lg-3 col-md-3 col-xs-12 sidebar">

<!-- 
          <ul class="list-group">
    <li class="list-group-item">Cras justo odio</li>
    <li class="list-group-item">Dapibus ac facilisis in</li>
    <li class="list-group-item">Morbi leo risus</li>
    <li class="list-group-item">Porta ac consectetur ac</li>
    <li class="list-group-item">Vestibulum at eros</li>
  </ul> -->
        

            
       <?php if($categories) { ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">Sub Categories</h4>
          </div>
          <div class="panel-body scrollpanel">
            <div class="list-group">       
           <?php foreach ($categories as $category) { ?>
           <?php  foreach ($category['children'] as $child) { ?>     
              <a href="<?php echo $child['href']; ?>" class="list-group-item"><?php echo $child['name']; ?></a>
              <?php } } ?>
            </div>
          </div>
        </div>

        <?php } ?>


      </div>

      <!-- sidebar end -->

      <!-- categories display -->

      <div class="col-lg-9 col-md-9 col-xs-12">

      <div class="row">
    <div class="col-xs-12 mb20">
    
    <article id="rmjs-1">
    <p class="cat-descript">Office supplies are an essential need for the everyday business or home office. We carry everything from paper clips, scissors and file folders to binders, staplers and many more essential office supplies. You’ll find what you need at Office Depot including office supplies for home, school or office. We carry the brands you know and trust.Filing and Storage Color code your documents with file folders or fasten papers inside the folder with reinforced tab fastener folders made from pressboard. We carry the brands you know and trust.Filing and Storage Color code your documents with file folders or fasten papers inside the folder with reinforced tab fastener Space, the final frontier. These are the voyages of the starship Enterprise. Its five year mission: to explore strange new worlds, to seek out new life and new civilizations, to boldly go where no man has gone before!Here's how it is: Earth got used up, so we terraformed a whole new galaxy of Earths, some rich and flush with the new technologies, some not so much. Central Planets, them was formed the Alliance, waged war to bring everyone under their rule; a few idiots tried to fight it, among them myself. I'm Malcolm Reynolds, captain of Serenity. Got a good crew: fighters, pilot, mechanic. We even picked up a preacher, and a bona fide companion. There's a doctor, too, took his genius sister out of some Alliance camp, so they're keeping a low profile. You got a job, we can do it, don't much care what it is.</p>
    </article>

    </div>


  </div>
        <div class="row">
          

<?php foreach($categories as $category) { 
foreach($category['children'] as $child ) {
?>
          <div class="col-md-4 col-sm-6 col-xs-12 mb30">
            <div class="main-product-cat">

                <div>
                  <a href="<?php echo $child['href']; ?>"><img src="<?php echo $child['image']; ?>" class="img-responsive"></a>
                </div>

                <div class="all-cat-btn">
                  <a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
                </div>
	

            </div>
          </div> 

<?php } } ?>
                
 

        </div>


      </div>

      <!-- categories end -->
      

  </div>
</div>


<?php echo $footer;?>
