
<?php echo $header; ?>

<div class="container mt20">

<!-- breadcrumb -->
  <div class="row">      
          <ol class="breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ol>      
  </div>

    <div class="row">
      <div class="col-xs-12">
         <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
      </div>
    </div>
</div>


  
<div class="container mb30">
  <div class="row">

  <div class="col-xs-12">
    <h4 class="sitecolor"><?php echo $heading_title; ?></h4>    
  </div>
     
      <div class="col-md-12 col-xs-12 addtables wishtables">
        <div class="table-responsive">

           <?php if ($products) { ?>
            <table class="table table-bordered">
                
              <thead>
                <tr>            
                  <th><?php echo $column_image; ?></th>
                  <th><?php echo $column_name; ?></th>
                  <th><?php echo $column_model; ?></th>
                  <th><?php echo $column_stock; ?></th>
                  <th><?php echo $column_price; ?></th>
                  <th><?php echo $column_action; ?></th>
                </tr>
              </thead>

              <tbody>
              <?php foreach ($products as $product) { ?>
                <tr>
                  <td>
                  <?php if ($product['thumb']) { ?>
                  <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
                  <?php } ?> 
                  </td>
                  <td><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></td>
                  <td><?php echo $product['model']; ?></td>
                  <td>
                  <span><?php echo $product['stock']; ?></span>
                  </td>
                  <td><?php if ($product['price']) { ?>
                  <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>
                <?php } else { ?>
                <b><?php echo $product['special']; ?></b> <s><?php echo $product['price']; ?></s>
                <?php } ?>
              <?php } ?></td>
                  <td class="qty">
                  <a onclick="cart.add('<?php echo $product['product_id']; ?>');" data-toggle="tooltip" title="<?php echo $button_cart; ?>"><img src="images/update-cart.png"></a>
                  <a href="<?php echo $product['remove']; ?>" data-toggle="tooltip" title="Remove" ><img src="images/remove-product.png"></a>
                  </td>
                </tr>
                <?php } ?>
              </tbody>

              </table>
               <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
   
        <div class="pull-right"><button class="btn btn-default buttons" onclick="location.href='<?php echo $continue; ?>'">CONTINUE</button></div>
  





            </div>
        </div>  


  </div>
 
</div>



<?php echo $footer; ?>
