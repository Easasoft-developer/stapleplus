<?php echo $header; ?>

<div class="container mt20">
<!-- breadcrumb -->
  <div class="row">      
          <ol class="breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ol>      
  </div>

  <div class="row">
    <div class="col-xs-12">
      <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
    </div>
  </div>

</div>

  
<div class="container mb30">  

    <div class="row">
      <div class="col-xs-12">
        <h4 class="sitecolor">My Account Information</h4>
      </div>
    </div>

  <div class="row">
      <div class="col-lg-9 col-md-9 col-xs-12">
     

    <div class="row myaccount">
      <div class="col-sm-6 col-xs-12">
        <div class="panel panel-danger">
          <!-- Default panel contents -->
          <div class="panel-heading"><strong><?php echo $text_my_account; ?></strong></div>
          
          <!-- List group -->
          <ul class="list-group">
            <li class="list-group-item"><a href="<?php echo $edit; ?>">Edit your account information</a></li>
            <li class="list-group-item"><a href="<?php echo $password; ?>">Change your password</a></li>
            <li class="list-group-item"><a href="<?php echo $address; ?>">Modify your address book entries</a></li>
            <li class="list-group-item"><a href="<?php echo $wishlist; ?>">Modify your wish list</a></li>
          </ul>
        </div>
      </div>

      <div class="col-sm-6 col-xs-12">
        <div class="panel panel-danger">
          <!-- Default panel contents -->
          <div class="panel-heading"><strong>My Orders</strong></div>
          
          <!-- List group -->
          <ul class="list-group">
            <li class="list-group-item"><a href="<?php echo $order; ?>">View your order history</a></li>
            <li class="list-group-item"><a href="<?php echo $return; ?>">View your return requests</a></li>
          </ul>
          <div class="panel-heading"><strong>Newsletter</strong></div>
          <ul class="list-group">
            <li class="list-group-item"><a href="<?php echo $newsletter; ?>">Subscribe / unsubscribe to newsletter</a></li>            
          </ul>
        </div>
      </div>
    </div>
  
  </div>      


     <?php echo $column_right; ?>




  </div>
</div>






<?php echo $footer; ?>
