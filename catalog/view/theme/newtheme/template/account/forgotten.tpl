<?php echo $header; ?>

<div class="container mt20">
<!-- breadcrumb -->
  <div class="row">      
          <ol class="breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ol>      
  </div>

    <div class="row">
      <div class="col-xs-12">
        <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
      </div>
    </div>
</div>

  
<div class="container mb30">
  <div class="row">

 

      <div class="col-lg-9 col-md-8 col-xs-12">
           
        <div class="row">      

        <div class="col-sm-12 col-md-10 col-lg-8">
          <div class="well">
            <h4>Forgot Your Password?</h4>
            <p>Enter the e-mail address associated with your account. Click submit to have your password e-mailed to you.</p>

            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >
              <div class="form-group">
                <label class="control-label" for="email-f">Your E-Mail Address</label>
                <input name="email" value="" placeholder="<?php echo $entry_email; ?>" id="email-f" class="form-control" type="email">
              </div>              
              <a class="btn btn-default buttons" href="<?php echo $back; ?>" role="button">CANCEL</a>
              <input type="submit" value="UPDATE" class="btn btn-default buttons" href="<?php echo $button_continue; ?>" role="button"  />
            </form>



          </div>
        </div>

      </div>
      </div>      


      <div class="col-lg-3 col-md-4 col-xs-12">
          <div class="list-group account-sidebar">
              <a href="<?php echo $login; ?>" class="list-group-item active">
              <span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Login</a>
              <a href="<?php echo $register; ?>" class="list-group-item">
              <span class="glyphicon glyphicon-registration-mark" aria-hidden="true"></span> Register</a>
              
            </div>
      </div>

  </div>
</div>





<?php echo $footer; ?>








