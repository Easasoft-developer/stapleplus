<?php echo $header; ?>
  <div class="container mt20">

<!-- breadcrumb -->
  <div class="row">      
    <ol class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ol>      
  </div>

  <div class="row">
  <div class="col-xs-12">  <?php if ($attention) { ?>
    <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?></div>
  </div>
</div>

<div class="container mb30">
  <div class="row">
      

   <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
      <div class="col-md-12 col-xs-12 addtables">
        <div class="table-responsive">
            <table class="table table-bordered">
                
              <thead>
                <tr>            
                  <th><?php echo $column_image; ?></th>
                  <th><?php echo $column_name; ?></th>
                  <th><?php echo $column_id; ?></th>
                  <th><?php echo $column_quantity; ?></th>
                  <th><?php echo $column_price; ?></th>
                  <th><?php echo $column_total; ?></th>
                </tr>
              </thead>

              <tbody>
              <?php foreach ($products as $product) { ?>
                
                <tr>
                  <td>
                  <?php if ($product['thumb']) { ?>
                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
                  <?php } ?>
                  </td>

                  <td><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                  <?php if (!$product['stock']) { ?>
                  <span>***</span>
                  <?php } ?>
                  <?php if ($product['option']) { ?>
                  <?php foreach ($product['option'] as $option) { ?>
                  <br />
                  <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                  <?php } ?>
                  <?php } ?>
                  <?php if ($product['reward']) { ?>
                  <br />
                  <small><?php echo $product['reward']; ?></small>
                  <?php } ?>
                  <?php if ($product['recurring']) { ?>
                  <br />
                  <span><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
                  <?php } ?>
                  </td>

                  <td><?php echo $product['id']; ?></td>

                  <td class="qty">
                  <input type="text" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>">
                  <button type="submit" data-toggle="tooltip" title="<?php echo $button_update; ?>"><img src="images/update-cart.png"></button>
                  <a type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" onclick="cart.remove('<?php echo $product['key']; ?>');"><img src="images/remove-product.png"></a>
                  </td>

                  <td><?php echo $product['price']; ?></td>
                  <td><?php echo $product['total']; ?></td>
                </tr>

                <?php } ?>
              </tbody>

              <tfoot>
                <tr>
                  <td colspan="6"><span><strong><?php echo $totals[0]['title']; ?>: <span><?php echo $totals[0]['text']; ?></strong></span></span></td>
                </tr>
              </tfoot>
            
              </table>
            </div>
        </div>  
</form>
</div>

       <div class="row"> 

<div class="col-xs-12"><?php if ($coupon || $voucher || $reward ) { ?>
      <h4><?php echo $text_next; ?>  (Coupon code is not applicable for Go Green products)</h4>
      <p><?php echo $text_next_choice; ?></p>
      <div class="panel-group" id="accordion"><?php echo $coupon; ?><?php echo $voucher; ?><?php echo $reward; ?></div>
      <?php } ?>
      <br></div>







        
        <div class="col-xs-12">
            <div class="" style="float: right;">
                    <?php 
            $matches='';
            foreach ($totals as $total) { 
              if(preg_match("/Total/", $total['title']) || preg_match("/Sub/", $total['title']) || preg_match("/Coupon/", $total['title'])){
              ?>
                  <strong><span><?php echo $total['title']; ?>:</span></strong>
                  <span><?php echo $total['text']; ?></span><br>
 <?php
            } 
            } ?>
              
                  
            </div>
                </div>
        </div>

  </div>
  <div class="container">
  <div class="row mtb20">
        <div class="col-sm-6 col-xs-7 couponbtn">
          <a onclick="location.href='<?php echo $continue; ?>'" type="submit" class="btn btn-default">CONTINUE SHOPPING</a>
        </div>

        <div class="col-sm-6 col-xs-5 couponbtn checkoutbtn" style="text-align: right;">
          <a onclick="location.href='<?php echo $checkout; ?>'" type="submit" class="btn btn-default">CHECKOUT</a>
        </div>
  </div>
</div>


<?php echo $footer; ?> 
   <script type="text/javascript">
          
        $(function() {
            $("[name=applycode]").click(function(){
                    $('.toHide').hide();
                    $("#blk-"+$(this).val()).show();
            });
         });                 
        
        </script>
