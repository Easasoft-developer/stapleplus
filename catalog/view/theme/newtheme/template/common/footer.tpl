</div>
<footer>

    <div class="container">
      <div class="row info">

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <h4>Help</h4>
          <ul class="list-unstyled">
            <li><a href="index.php?route=information/information&information_id=8">Faq</a></li>
            <li><a href="index.php?route=information/information&information_id=9">Payments</a></li>
            <li><a href="index.php?route=information/information&information_id=10">Return and Exchange</a></li>
            <li><a href="index.php?route=information/information&information_id=11">Careers</a></li>
            
          </ul>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <h4>Staple Plus</h4>
          <ul class="list-unstyled">

            <li><a href="<?php echo $contact; ?>">Contact Us</a></li>
               <li><a href="index.php?route=information/information&information_id=4">About Us</a></li>
            <li><a href="index.php?route=information/information&information_id=5">Terms & Conditions</a></li>
            <li><a href="index.php?route=information/information&information_id=3">Privacy Policy</a></li>
            <!-- <li><a href="index.php?route=information/information&information_id=7">Returns Policy</a></li> -->
          </ul>
        </div>

       

       <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <h4>My Account</h4>
          <ul class="list-unstyled">
            <li><a href="<?php echo $account; ?>">My Account</a></li>
            <li><a href="<?php echo $order; ?>">Order History</a></li>
            <li><a href="<?php echo $wishlist ?>">Wishlist</a></li>
            
          </ul>
        </div>


           <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 subscribe">
          <h4>Subscribe</h4>




              <form class="navbar-form" name="subscribe" id="subscribe" >
              
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" name="subscribe_email" id="subscribe_email" class="form-control subscribe-mail" placeholder="Email">
                        <span class="input-group-btn">
                        <button class="btn btn-default subscribe-btn" type="button" onclick="email_subscribe()" >Submit</button>
                        </span>
                      </div>
                    </div>
                <p id="subscribe_result" style="text-align:center; font-size:14px"></p>
      
      </form>
         

        </div>
        
      </div>

      <div class="row payment">

          <div class="col-lg-2 col-md-2 col-xs-12">
            <ul class="social list-inline"> 
                <li><a href="https://www.facebook.com/"><img src="images/fb.png" alt="" /></a></li>
                <li><a href="https://www.twitter.com/"><img src="images/tw.png" alt="" /></a></li>
                <li><a href="https://www.plus.google.com/"><img src="images/gp.jpg" alt="" /></a></li>
                <li><a href="https://www.youtube.com/"><img src="images/yt.jpg" alt="" /></a></li>
              </ul>
          </div>       

   

        </div>

        <div class="row copyrights">
        <div class="col-xs-12">
          <h5 class="text-center">Copyright &copy; 2015 - <a href="">Staple Plus</a></h5>
        </div>        
      </div>
        

      </div>
    

  </footer>




    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <!-- Include all compiled plugins (below), or include individual files as needed -->
	
 <!-- Include js plugin -->

<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<script src="catalog/view/javascript/mediaresponsive.js" type="text/javascript"></script>
 <script type="text/javascript" src="catalog/view/javascript/js/jquery.simplegallery.js"></script>
 <script type="text/javascript" src="catalog/view/javascript/js/jquery.simplelens.js"></script>

  <script type="text/javascript">
    $('article').readmore({
      collapsedHeight: 110,
        afterToggle: function(trigger, element, expanded) {
          if(! expanded) { // The "Close" link was clicked
            $('html, body').animate( { scrollTop: element.offset().top }, {duration: 200 } );
          }
        }
      });
    </script>

    <script type="text/javascript">
    document.querySelector( ".navbar-toggle" )
        .addEventListener( "click", function() {
          this.classList.toggle( "active" );
        });
    </script>


    <script type="text/javascript">
    
    // $("#clearallcat > div:nth-child(3n)").after("<div class='clearfix visible-lg-block visible-md-block'></div>");
    // $("#products > div:nth-child(3n)").after("<div class='clearfix visible-lg-block visible-md-block'></div>");
    // $("#products").find("div:nth-child(3n)").after("<div class='clearfix visible-lg-block visible-md-block'></div>");
    // $("#clearallcat > div:nth-child(2n)").after("<div class='clearfix visible-sm-block'></div>");
    </script>


    <!-- Include js plugin <script src="js/readmore.js"></script>-->

   <script type="text/javascript">
$(document).ready(function() {
    $('#list').click(function(event){
    event.preventDefault();
    $('#products .item').addClass('list-group-item');
     $('#grid').removeClass("current");
     $('#list').addClass("current");
  });

    $('#grid').click(function(event){
      event.preventDefault();
      $('#products .item').removeClass('list-group-item');
      $('#products .item').addClass('grid-group-item');
       $('#list').removeClass("current");
     $('#grid').addClass("current");
    });
});   
</script>
         

<script language="javascript">
  function email_subscribe(){
    // alert($("#subscribe").serialize());
    $.ajax({
        type: 'post',
        url: 'index.php?route=module/newslettersubscribe/subscribe',
        dataType: 'html',
              data:$("#subscribe").serialize(),
        success: function (html) {
          eval(html);
        }}); 
  }
  </script>





<script type="text/javascript">
    // $(".feature > .col-sm-3:nth-child(5)").after("<div class='clearfix visible-sm-block visible-md-block visible-lg-block'></div>");
    </script>

    <script>

    // $(if (screen.width >= 768) {
      $('.megamenu-toggle').click(function() {
          var location = $(this).attr('href');
          window.location.href = location;
          return false;
          });
    // });
    

    </script>

   
     
      <script type="text/javascript" src="catalog/view/javascript/js/slick.js"></script>
      <script type="text/javascript">
        $(document).ready(function(){
         $('.autoplay').slick({
          slidesToShow: 4,
          slidesToScroll:1,
          autoplay: false,          
          responsive: [
                {
                  breakpoint: 1200,
                  settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                  }
                },
                {
                  breakpoint: 992,
                  settings: {
                    slidesToShow:3,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow:3,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 640,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 481,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }

              ]
        });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
         $('.trends').slick({
          slidesToShow: 4,
          slidesToScroll:1,
          autoplay: false,         
          responsive: [
                {
                  breakpoint: 1200,
                  settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                  }
                },
                {
                  breakpoint: 992,
                  settings: {
                    slidesToShow:3,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow:3,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 640,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 481,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }

              ]
        });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
         $('.ads').slick({
          dots: true,
          prevArrow: false,
          nextArrow: false,
          slidesToShow: 3,
          slidesToScroll:1,
          autoplay: false,         
          responsive: [
                {
                  breakpoint: 1200,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                  }
                },
                {
                  breakpoint: 992,
                  settings: {
                    slidesToShow:3,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow:2,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 640,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 481,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }

              ]
        });
        });
    </script>





</body></html>
