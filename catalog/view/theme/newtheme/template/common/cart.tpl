<div id="cart" class="st-acc-li-ca dropdown">
<a class="icons-a"><img src="images/cart.png"></a>
<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-shopping-cart visible-xs-inline"></i> Cart</a>
<!--   <button type="button" data-toggle="dropdown" class="btn btn-default btn-block dropdown-toggle">  <span id="cart-total"><?php echo $text_items; ?></span> </button> -->
  <ul class="dropdown-menu pull-right">
    <?php if ($products || $vouchers) { ?>
    <li>
      <table class="table table-striped">
       <thead style="background: lightgray;font-weight:bold;">
        <tr class="text-center">
        <td>Product</td>
        <td>Quantity</td>
        <td>Price</td>
        <td>Remove</td>       
        </tr>        
      </thead>

        <?php foreach ($products as $product) { ?>
       
        <tr>
         
          <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
           
            <?php if ($product['recurring']) { ?>
            <br />
            - <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
            <?php } ?></td>
          <td class="text-center"><?php echo $product['quantity']; ?></td>
          <td class="text-center"><?php echo $product['total']; ?></td>
          <td class="text-center"><button type="button" onclick="cart.remove('<?php echo $product['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>
        </tr>
        <?php } ?>
        <?php foreach ($vouchers as $voucher) { ?>
        <tr>
          <td class="text-center"></td>
          <td class="text-center"><?php echo $voucher['description']; ?></td>
          <td class="text-center">x&nbsp;1</td>
          <td class="text-center"><?php echo $voucher['amount']; ?></td>
          <td class="text-center text-danger"><button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>
        </tr>
        <?php } ?>
      </table>
    </li>
    <li>
    <div class="row">
      <div class="col-sm-4">
        <a href="<?php echo $cart; ?>" class="btn btn-default text-uppercase"> <?php echo $text_cart; ?></a>
      </div>
      <div class="col-sm-4 col-sm-offset-4">
        <a href="<?php echo $checkout; ?>" class="btn btn-default text-uppercase"> <?php echo $text_checkout; ?></a>
      </div>
    </div>
    <!-- <table>
      <tr>
        <td><a href="" class="btn btn-default"> <?php echo $text_cart; ?></a></td>
        <td><a href="" class="btn btn-default"> <?php echo $text_checkout; ?></a></td>
      </tr>
    </table> -->
        <!-- <a href="" class="btn btn-default"> <?php echo $text_cart; ?></a>
          <a href="" class="btn btn-default"></i> <?php echo $text_checkout; ?></a> -->
      <!-- <div>
        <a href="" class="btn btn-default"><i class="fa fa-share"></i> <?php echo $text_checkout; ?></a>
        <p class=""><a href="<?php echo $cart; ?>"><strong><i class="fa fa-shopping-cart"></i> <?php echo $text_cart; ?></strong></a>&nbsp;&nbsp;&nbsp;<a href="<?php echo $checkout; ?>"><strong><i class="fa fa-share"></i> <?php echo $text_checkout; ?></strong></a></p>
      </div> -->
    </li>
    <?php } else { ?>
    <li>
      <p class="text-center"><?php echo $text_empty; ?></p>
    </li>
    <?php } ?>
  </ul>
</div>
