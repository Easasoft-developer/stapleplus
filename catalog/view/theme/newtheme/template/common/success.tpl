<?php echo $header; ?>

<div class="container mt20">
<!-- breadcrumb -->
  <div class="row">      
          <ol class="breadcrumb">
           <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
             <?php } ?>
          </ol>      
  </div>
</div>


<div class="container mb30">
  <div class="row">
      <?php echo $column_left; ?>
    <?php echo $content_top; ?>
      <div class="col-lg-9 col-md-8 col-xs-12">
          <h4><?php echo $heading_title; ?></h4> 
          <p><?php echo $text_message; ?></p>
        
      </div>      


   <?php echo $content_bottom; ?>
    <?php// echo $column_right; ?>
 <div class="col-lg-3 col-md-4 col-xs-12">
          <div class="list-group account-sidebar">
              <a href="<?php echo $login; ?>" class="list-group-item active">
              <span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Login</a>
              <a href="<?php echo $register; ?>" class="list-group-item">
              <span class="glyphicon glyphicon-registration-mark" aria-hidden="true"></span> Register</a>
              
            </div>
      </div>

  </div>
</div>


<?php echo $footer; ?>
