<?php echo $header; ?> 

	
<div class="container">
	<!--<div class="row">
			<div class="col-sm-12">
				<h3 class="servicehead">Services Offered</h3>
			</div>
	</div> -->
	<div class="row">   
          <ol class="breadcrumb">
                      <li><a href="">Home</a></li>
                        <li><a href="index.php?route=common/service">Services</a></li>
                      </ol>      
  </div>



	<div class="row">
	<div class="col-sm-12">
	<p>StaplePlus Tech Services take the stress and anxiety out of your technology purchases.
	
	Whichever scale of business you carry out, we've got a great range of Tech Service solutions to 
	
	make your life easier.</p>

<p>We work in partnership with the technology experts, so you can enjoy the convenience of 

having technical support whenever you need it. With great advice, installation assistance, 

software support and more, there's no technology problem we can't solve.</p>

<p>Few Advanced IT Solutions offered by Stapleplus;</p>
<ul>
	<li>Onsite Annual Maintenance Agreements*</li>
	<li>Cabling Infrastructure Solutions</li>
	<li>IT Consulting, Design & Implementation</li>
	<li>Voice & Data Solutions</li>
	<li>Security Solutions</li>
	<li>Access Control (Standard & Biometric)</li>
	<li>CCTV surveillance (Full video monitoring of your operation)</li>
	<li>Alarm systems for Houses & Businesses</li>
	<li>Network Security</li>
	<li>Anti Virus Security</li>
	<li>Firewalls & Intrusion Protective/Prevention</li>
</ul>
<p>
Tech can be complicated & that's when our experts reach you. We'll solve the problem and get you back 

to work. So whether you need tech support online or at your home or office, Staple Plus Tech Services is 

here for you.</p>

<p>Talk to one of our tech experts today. Give us a call at 00971503082775 or <a href="mailto:info@stapleplus.com">info@stapleplus.com</a></p>

</div>
    <div class="col-xs-12">
      <div class="row">
	       <?php foreach ($servicelist as $slist) { ?>

					<div class="col-sm-3">
						 <div class="thumbnail text-center">
				      <i class="sitecolor fa fa-check-square-o fa-3x"></i>
				      <div class="caption">
				        
				        <h4><?php echo $slist['service_list']; ?></h4>
				        
				      </div>
				    </div>

					</div>

				<?php } ?>      	
      
     
    </div>
    </div>
  </div>


</div>







<?php echo $footer; ?>