<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --> <!-- WARNING: Respond.js doesn't work if you view the page via file:// --> <!--[if lt IE 9]> <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script> <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> <![endif]-->

<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha256-k2/8zcNbxVIh5mnQ52A0r3a6jAgMGxFJFE2707UxGCk= sha512-ZV9KawG2Legkwp3nAlxLIVFudTauWuBpC10uEafMHYL0Sarrz5A7G79kXh5+5+woxQ5HM559XX2UZjMJ36Wplg==" crossorigin="anonymous">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300italic,400italic' rel='stylesheet' type='text/css'>
<link href="catalog/view/theme/newtheme/stylesheet/jquery.simplegallery.css" rel="stylesheet">
<link href="catalog/view/theme/newtheme/stylesheet/jquery.simplelens.css" rel="stylesheet">

<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>


<script type="text/javascript">
$(document).ready(function() {


$.ajax({ 

type: "post",
url: "index.php?route=product/ajaxcall",
dataType: "json",
success: function(json) {

if(json['slider']) {
var sliImg = json.slider;
var html5 = ''; 
$.each(sliImg, function(xyz, sliderImg) {
if(xyz == 0 ) {
html5 += '<div class="item active"><img src="'+sliderImg.image+'"></div>';
}
else { html5 += '<div class="item"><img src="'+sliderImg.image+'"></div>'; }
});

$('.carousel-inner').html(html5);

}

}

});




$('.close').click(function() {

$.ajax({ 

type: "Post",
url: "index.php?route=product/ajaxcall",
dataType: "json"

});

});


$("#bs-example-navbar-collapse-1").hover(function(){
$.ajax({
type: "post",
url: "index.php?route=product/ajaxcall",
dataType: "json",
success: function(json) {


if(json['inkntoner']) {

	var iandt = json.inkntoner;
	var html3 = '';
$.each(iandt,function(k,intele) {
html3 += '<div class="col-sm-2"><a  href=""><img src="'+intele.image+'" class="img-responsive"></a></div>';
});


$('.inkntoner').html(html3);
}


if(json['brands']) {

var fb = json.brands;
var html ='';
$.each(fb,function(i,ele){

html += '<li>'+'<img src="'+ele.image+'">'+'</li>'; 
});
$('.bimage').html(html);

}



else{
alert('error');
}


if (json['onefeatured']) {
var ofb = json.onefeatured;
var html2 = '';
$.each(ofb,function(j,ofele){ 
html2 += '<div class="onefeatured"><a href="index.php?route=product/product&product_id='+ofele.product_id+'"><img src="image/'+ ofele.image+'" class="img-responsive"></a></div><a href="index.php?route=product/product&path=27&product_id='+ofele.product_id+'"><button class="btn btn-default" type="submit">Shop Now<span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span></button></a>' 
});
$('.onefeatured').html(html2);
}


}



});
});
});
</script>


<script src="catalog/view/javascript/bootstrap/js/readmore.min.js" type="text/javascript"></script>
<link href="catalog/view/theme/newtheme/stylesheet/stylesheet.css" rel="stylesheet">
<link href="catalog/view/theme/newtheme/stylesheet/responsive.css" rel="stylesheet">
<link href="catalog/view/theme/newtheme/stylesheet/slick.css" rel="stylesheet">
<link href="catalog/view/theme/newtheme/stylesheet/slick-theme.css" rel="stylesheet">

<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>


<?php echo $google_analytics; ?>
</head>
<body>



    <header>
      <div class="top">
        <div class="container">
          <div class="row">
              <div class="col-lg-5 col-md-5 col-sm-4 col-xs-12">
                <ul class="list-inline">
                  <li><span class="hidden-xs">Tel :</span><i class="fa fa-phone visible-xs-inline"></i> 00971503082775</li>
                  <li><span class="hidden-xs">Email :</span><i class="fa fa-envelope visible-xs-inline"></i> <a href="mailto:info@stapleplus.com">info@stapleplus.com</a></li>
                </ul>
              </div>
             <div class="col-lg-6 col-lg-offset-1 col-md-7 col-sm-8 col-xs-12">
                <ul class="list-inline account">
                  <li><a href="index.php?route=corporate">Corporate/Government</a></li>
                  <li><a href="index.php?route=orderbyitem">Order by Item #</a></li>
                  <li><a href="<?php echo $order; ?>">Track Your Order</a></li>

                   <?php if (!$logged) { ?>
	<li class="dropdown login-dd"><a href="" class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Login or Register</a>

                    <div class="dropdown-menu login-dropdown">
                        <div class="col-md-6">
                          <h4>Login or Register</h4>  
            <form  action="<?php echo $action; ?>"  method="post" enctype="multipart/form-data">
              <div class="form-group">

                <input name="email" value="<?php echo $email; ?>" placeholder="E-Mail Address" id="input-email" class="form-control" type="text">
              </div>
              <div class="form-group">

                <input name="password" value="<?php echo $password; ?>" placeholder="Password" id="input-password" class="form-control" type="password"></div>
                <a href="<?php echo $forgotten; ?>">Forgotten Password</a>
<input type="submit" value="Sign In" class="btn btn-default buttons" href="<?php echo $button_login; ?>" />

                <?php if ($redirect) { ?>
              <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
              <?php } ?>
                          </form>
                        </div>                      
                        <div class="col-md-6">
                          <h4>Register</h4>
                          <p>Create an account to make your shopping experience easier.</p>
                          <a href="<?php echo $register; ?>" class="btn btn-default buttons">Register</a>
                        </div>
                    </div>
                  </li>
                 <?php } else { ?>
                <li><a href="<?php echo $logout; ?>">Logout</a></li>
                 <?php } ?>



                </ul>
            </div>
          </div>            
        </div>
        <!-- end of top container -->
      </div>


      <!-- end of top bar and start middle part -->



      <div class="middle">
        
        <div class="container">
          <div class="row logo-srh-icn">

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
              <div id="logo">
          <?php if ($logo) { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>

            </div>

           <div class="col-lg-6 col-md-5 col-sm-4 col-xs-12 search">
              <h5><strong><i class="fa fa-truck"></i> <a href="index.php?route=information/information&information_id=12">Free Delivery On AED 250.00</a></strong></h5>
                  <form class="navbar-form" role="search">
                  <?php echo $search;?>  

                  </form>

            </div> 
             <div class="col-lg-3 col-md-4 col-sm-5 col-xs-12">
              <ul class="list-inline icons">
              <!-- stores-account-list-cart -->
                <li>
                <div class="st-acc-li-ca dropdown">
                <a href="index.php?route=storefinder" class="icons-a"><img src="images/storespoint.png"/></a>
              <a  class="dropdown-toggle" type="button" id="stores"  aria-haspopup="true" aria-expanded="false" ><i class="fa fa-map-marker visible-xs-inline"></i> Stores <span class="glyphicon glyphicon-chevron-down hidden-xs" aria-hidden="true"></span></a>

                 <div class="dropdown-menu find-store" aria-labelledby="stores">
                    <h4>Store Locator</h4>                  
                  
                    <form class="navbar-form" role="search" name="form_search_action">
                      <div class="input-group">
                        <input type="text" class="form-control" placeholder="Enter your zip code" name="store_search1">
                        <div class="input-group-btn" id="store_search1">

                            <button type="button" id="fsearch" class="btn btn-default" >Find</button>

                        </div>
                        
                      </div>
                    </form>
                  </div> 
                
                </div>
                </li>    

                 <li>
                <div class="st-acc-li-ca dropdown">
                <a class="icons-a"><img src="images/account.png"/></a>
                <a class="dropdown-toggle" type="button" id="account"  aria-haspopup="true" aria-expanded="false" href="<?php echo $account; ?>"><i class="fa fa-user visible-xs-inline"></i> Account <?php if ($logged) { ?> <span class="glyphicon glyphicon-chevron-down hidden-xs" aria-hidden="true"></span><?php } ?></a>
			 <?php if ($logged) { ?>             
		     <div class="dropdown-menu find-store" aria-labelledby="account">
	        <button onclick="location.href='<?php global $url; echo $url->link('product/compare'); ?>'" class="btn btn-danger btn-sm btn-block">Product Compare</button>
                  <button onclick="location.href='<?php echo $order; ?>'" class="btn btn-danger btn-sm btn-block">Order History</button>
                  <button onclick="location.href='<?php echo $transaction; ?>'"  class="btn btn-danger btn-sm btn-block">Transactions</button>
                  <button onclick="location.href='<?php echo $download; ?>'"  class="btn btn-danger btn-sm btn-block">Downloads</button>

                </div> 
<?php } ?>
                </div>
                </li>
        <li>
                <div class="st-acc-li-ca dropdown">
                <a class="icons-a"><img src="images/mylist.png"/></a>
                <a href="<?php echo $wishlist ?>"><i class="fa fa-list visible-xs-inline"></i>  Wishlist </a>
                </div>
                </li>

                  <li class="carts">
                   <?php echo $cart; ?> 
                  </li>

              </ul>
            </div>
          </div>
          </div>  
        <!-- end of middle container -->
               






<!-- --------------------------------------------  Menu | wm-3 ----------------------------------------------------    -->
     

          <div class="menu container">
            <nav class="navbar navbar-default">
              <div class="">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    
                  </button>
                  <!-- <a class="navbar-brand hidden-sm hidden-md hidden-lg" href="#"><img src="images/logo.png" class="img-responsive" /></a> -->
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                

                 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                  <!--<li><a href="http://easasoft.com/stapleplus/index.php?route=allcategories">Office Supplies</a></li>-->

        <?php 
	
	foreach ($categories as $category) {  ?>

        <?php if ($category['children'] || $category['id'] == 17 ) { 
          $newarray = array_slice($category['children'],0, 7);   ?>
                    <li class="dropdown">



                    <div class="dropdown-menu mega-dropdown-menu megamenu">
                        <div class="col-md-9 col-sm-12 col-xs-12">
                          <div class="row feature">
		<!-- Temp -->		<?php if($category['id'] == 57) {  ?>
                            <div class="col-sm-12">
                              <h4>Search by Cartridge or Printer</h4>
                            </div>
                            <div class="col-sm-12 inksearch">  
                              
                              <ul class="nav nav-pills" role="tablist">
                                <li role="presentation" class="active"><a href="#cartridge" aria-controls="cartridge" role="tab" data-toggle="tab">Cartridge</a></li>
                                <li role="presentation"><a href="#Printer" aria-controls="Printer" role="tab" data-toggle="tab">Printer</a></li>                                
                              </ul>

                              <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="cartridge">
                                  <div id="searchofcartridge" class="input-group">
                                    <input type="text" name="searchofcartridge" value="" placeholder="Search for Cartridge" class="form-control" />
                                    <span class="input-group-btn">
                                      <button type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </span>
                                  </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="Printer">
                                    <div id="searchofprinter" class="input-group">
                                    <input type="text" name="searchofprinter" value="" placeholder="Search for Printer" class="form-control" />
                                    <span class="input-group-btn">
                                      <button type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </span>
                                  </div>
                                </div>                        
                              </div>

                            </div>
                            
                            <div class=" col-xs-12">
                              <div class="row">
                                <div class="col-xs-6">
                                  <h4>Featured Brands</h4>
                                </div>
                                <div class="col-xs-6">
                                  <a class="inklink" href="<?php echo $category['href'] ?>" >See All Brands in Ink & Toner</a>
                                </div>
                                <div class="clearfix mt10"></div>
                               
                                  <div class="inkntoner">
                                    <div class="col-sm-2">
                                    <a ><img src="" class="img-responsive"></a> 
                                    </div>                  
                                  </div>
                               
                              </div>
                              
                            </div>
				                    


			
			<!-- Temp -->		<?php } else { ?>
                            <div class="col-sm-12">
                              <h4>Featured Categories</h4>
                            </div>
                                   <?php foreach (array_chunk($newarray, ceil(count($newarray) )) as $children) { ?>
                                 <?php foreach ($children as $child) { ?>  
                                    <?php if($child['image']) { ?>                                      
                            <div class="col-sm-3">
                              <div class="subfeature">
                                <a href="<?php echo $child['href']; ?>"><img src="<?php echo $child['image'];?>" class="img-responsive"></a>
                                <p><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></p>
                              </div>
                            </div>

                            <?php } } } ?>

         
         <?php if($category['id'] != 17) { ?>
                            <div class="col-sm-3">
                              <div class="subfeature">
                                <a href="<?php echo $category['href']; ?>"><img src="images/all-categories-arrow.png" class="img-responsive"></a>
                                <p><a href="<?php echo $category['href']; ?>"><?php echo $text_all; ?> <?php echo $category['name']; ?></a></p>
                              </div>
                            </div>
                            <?php } } ?>
  <!-- Temp -->        
                          <?php  if($category['id'] == 17) { ?>
                      <div class="col-sm-3">
                              <div class="subfeature">
                                <a href="index.php?route=common/service"><img src="images/service.png" class="img-responsive" height="150" width="150"></a>
                                <p><a href="index.php?route=common/service"><?php echo $text_all; ?> <?php echo $category['name']; ?></a></p>
                              </div>
                            </div>

<?php } ?> 
                          </div>
		<!-- Temp -->		<?php if($category['id'] != 57) {  ?>
                         <div  class="row brands">
                            
                            <div  class="fbrands col-xs-12">
                              <h4>Featured Brands</h4>
                            </div>

                            <ul  class="bimage list-inline">
                              <img src="images/brand1.jpg">
                            </ul>
                            
                          </div>
                <!-- Temp -->          <?php } ?>

			
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12 dollardeals">
                          <h4>Shop for discount product</h4>
                          <p>Discount based on price or quantity for any product. </p>


   <div class="onefeatured">
        <a href=""><img src=""  class="img-responsive"></a>
      </div>



                        </div>
                        
                      </div>

                      <a href="<?php  if($category['id'] == 17 ) { echo 'index.php?route=common/service';} else { echo $category['href']; }?>" class="dropdown-toggle megamenu-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $category['name']; ?><span class="sr-only">(current)</span></a>



                    </li>

                      <?php } else { ?>
                      <li>
                      <a href="<?php echo $category['href1'];  ?>"><?php echo $category['name']; ?></a>
                      </li>
                     <?php } }?>
                    
                  </ul>
                </div><!-- /.navbar-collapse -->

              </div><!-- /.container-->
            </nav>
          </div>
          <!-- ............................ Wm-3 ........................................... -->


</div>

</header>

<?php
if($_SESSION['user'] != 'me') { ?>

<div class="specialbanner">
<div class="container">
    
      
      <div class="alert row fade in" role="alert">
  
      <div class="col-sm-4 col-xs-12">
        <h2><strong>Ultimate <span class="sitecolor">Sale</span> Event</strong></h2>
      </div>
      <div class="col-sm-4 col-xs-12">
        <h4>Office Supplies, Paper, Ink & Toner, Breakroom <br> & much more...</h4>
      </div>
      <div class="clearfix visible-xs-block"></div>
      <div class="col-sm-2 col-xs-4">
        <a href="index.php?route=product/special" class="btn btn-default">SHOP NOW</a>
      </div>
      <div class="col-sm-2 col-xs-8">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>

    </div>

</div>
</div>
  <?php } ?>




   <div class="container-fluid slider">
     <div class="row">
      <?php
           $url = 'easasoft.com'."$_SERVER[REQUEST_URI]";
            
            if( ($url == 'easasoft.com/stapleplus/index.php?route=common/home') || ($url == 'easasoft.com/stapleplus/'))

            {
            ?>
       <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">

            <div class="item active"><img src="images/slider2.jpg" alt="..."></div>
            
          </div>

 
        </div>



        <div class="container">
        <div class="row">
        <!-- <div class="col-sm-2"> 
          <figure class="mt20">
            <img src="images/freetruck.png" class="img-responsive">
          </figure>
        </div> -->
            
            <div class="col-sm-12">
              <h2 class="">Free Delivery on <span class="">ALL Ink Toner</span>. No minimum Purchase Required. *</h2>
            </div>
        </div>
      </div>
          <?php } ?>
        
     </div>
   </div>  
   <!-- ................end of slider and starting main part................... -->

  



<div class="main">
