<?php echo $header; ?>

<div class="main">

  
<div class="container mtb20">
<!-- breadcrumb -->
  <div class="row">      
          <ol class="breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ol>      
  </div>
</div>



<?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>





  
<div class="container mb30">
  <div class="row">

      <div class="col-lg-9 col-md-9 col-xs-12">
           
        <div class="row">

        <div class="col-sm-6">
          <div class="well">
            <h4><?php echo $text_new_customer; ?></h4>
            <p><strong><?php echo $text_register; ?></strong></p>
            <p><?php echo $text_register_account; ?></p>
            <a class="btn btn-default buttons" href="<?php echo $register; ?>" role="button"><?php echo $button_continue; ?></a>
        </div>
        </div>

        <div class="col-sm-6">
          <div class="well">
            <h4><?php echo $text_returning_customer; ?></h4>
            <p><strong><?php echo $text_i_am_returning_customer; ?></strong></p>
            <form  action="<?php echo $action; ?>"  method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                <input kl_virtual_keyboard_secure_input="on" name="email" value="<?php echo $email; ?>" placeholder="E-Mail Address" id="input-email" class="form-control" type="text">
              </div>
              <div class="form-group">
                <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
                <input kl_virtual_keyboard_secure_input="on" name="password" value="<?php echo $password; ?>" placeholder="Password" id="input-password" class="form-control" type="password">
                <a href="<?php echo $forgotten; ?>">Forgotten Password</a></div>
              <input type="submit"  palceholder="CONTINUE" class="btn btn-default buttons" href="<?php echo $button_login; ?>" role="button" />
                <?php if ($redirect) { ?>
              <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
              <?php } ?>
                          </form>
          </div>
        </div>

      </div>
      </div>      



     <?php echo $column_right; ?>
  </div>
</div>




</div>


<?php echo $footer; ?>