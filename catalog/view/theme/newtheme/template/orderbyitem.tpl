<?php echo $header;?>


<div class="container mt20">
<!-- breadcrumb -->
  <div class="row">      
          <ol class="breadcrumb">
            <li><a href="">Home</a></li>           
            <li><a href="index.php?route=orderbyitem"> Order by Item</a></li>
          </ol>      
  </div>
</div>

  
<div class="container mb30">
  <div class="row">

      <div class="col-xs-12">
       <h4 class="sitecolor">Order By Item Number</h4>
       <h3><i class="fa fa-arrow-circle-down"></i> Here you can add bulk orders by knowing the item numbers.</h3>
      </div>

      <div class="col-xs-12 orderbyitem">
        <div class="panel panel-default">
          <div class="panel-heading">Enter Item Number(s) & Quantities</div>
          <div class="panel-body">
                 
           
            <form class="orderbyitem" id="orderbyitem">
            <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <td style="width: 90%;"><strong>Item #</strong></td>
                  <td><strong>Quantity</strong></td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><input type="tel" name="product_id[]" maxlength="20" class="form-control" placeholder=""></td>
                  <td><input type="tel" name="quantity[]" maxlength="4" class="form-control quantityitem" placeholder=""></td>
                </tr>
            
                <tr>
                  <td><input  type="tel" name="product_id[]" maxlength="20" class="form-control" placeholder=""></td>
                  <td><input type="tel" name="quantity[]" maxlength="4" class="form-control quantityitem" placeholder=""></td>
                </tr>
                <tr>
                  <td><input type="tel" name="product_id[]" maxlength="20" class="form-control" placeholder=""></td>
                  <td><input type="tel" name="quantity[]" maxlength="4" class="form-control quantityitem" placeholder=""></td>
                </tr>
                <tr>
                  <td><input type="tel" name="product_id[]" maxlength="20" class="form-control" placeholder=""></td>
                  <td><input type="tel" name="quantity[]" maxlength="4" class="form-control quantityitem" placeholder=""></td>
                </tr>
                <tr>
                  <td><input type="tel" name="product_id[]" maxlength="20" class="form-control" placeholder=""></td>
                  <td><input type="tel" name="quantity[]" maxlength="4" class="form-control quantityitem" placeholder=""></td>
                </tr>
                <tr>
                  <td><input type="tel" name="product_id[]" maxlength="20" class="form-control" placeholder=""></td>
                  <td><input type="tel" name="quantity[]" maxlength="4" class="form-control quantityitem" placeholder=""></td>
                </tr>
                <tr>
                  <td><input type="tel" name="product_id[]" maxlength="20" class="form-control" placeholder=""></td>
                  <td><input type="tel" name="quantity[]" maxlength="4" class="form-control quantityitem" placeholder=""></td>
                </tr>
                <tr>
                  <td><input type="tel" name="product_id[]" maxlength="20" class="form-control" placeholder=""></td>
                  <td><input type="tel" name="quantity[]" maxlength="4" class="form-control quantityitem" placeholder=""></td>
                </tr>
                <tr>
                  <td><input type="tel" name="product_id[]" maxlength="20" class="form-control" placeholder=""></td>
                  <td><input type="tel" name="quantity[]" maxlength="4" class="form-control quantityitem" placeholder=""></td>
                </tr>
                <tr>
                  <td><input type="tel" name="product_id[]" maxlength="20" class="form-control" placeholder=""></td>
                  <td><input type="tel" name="quantity[]" maxlength="4" class="form-control quantityitem" placeholder=""></td>
                </tr>
                <tr id="addmorelist">
                  <td><input type="tel" name="product_id[]" class="form-control" maxlength="20" placeholder=""></td>
                  <td><input type="tel" name="quantity[]" maxlength="4" class="form-control quantityitem" placeholder=""></td>
                </tr>
                
              </tbody>
              <tfoot>
                <tr>
                  <td><a class="wishlist btn btn-default">ADD TO WISHLIST</a></td>
                  <td><a class="cart btn btn-default">ADD TO CART</a></td>                  
                </tr>
              </tfoot>
            </table>
            </div>

            <a  class="addmore">Enter more items <i class="fa fa-plus-circle"></i></a>
              
            </form>
          </div>
        </div>
      </div>     



      

  </div>
</div>





<script type="application/javascript">
$(document).ready(function() {




    $("a.addmore").click(function(){
      $("tr#addmorelist").after("<tr><td><input type='tel' maxlength='20'  name='product_id[]' class='form-control'></td><td><input type='tel' name='quantity[]' maxlength='4' class='form-control quantityitem'></td><tr>");
    });

    $(".qtyitem").focus(function(){
      $(this).next("#hidden").css("display","block");
    });

  

    $(".quantityitem").keypress(function (e) {
  
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               return false;
    }
   });



$('.orderbyitem').on('click','.close',function() {
  $(this).parents('td').find("input").val("");
    $(this).parents('td').find("input").css('display','block');
    
    $(this).closest(".hideitem").remove();
    return false;

});


 var product_id = new Array();

var assignquantity = new Array();

// $('input').change(function(){ 
  
// $.each($("input[name='product_id[]']"), function() {
//   if($(this).val()!=''){
//   product_id.push($(this).val());  
// }
// });

// $.each($("input[name='quantity[]']"), function() {
//    if($(this).val()!=''){
//   assignquantity.push($(this).val()); 
//   } 
// });

});


$('.cart').on('click',function(){

  
// alert();
// var product_id=$("input[name='product_id[]']").val();
// var assignquantity=$("input[name='quantity[]']").val();
  $.ajax({
      url: 'index.php?route=checkout/cart/addorders',
      type: 'post',
      data: $('form#orderbyitem').serialize(),
      dataType: 'json',   
      success: function(json) {
    
      // $('#product_quantity1'+qul).val('');

        $('.alert, .text-danger').remove();

        if (json['redirect']) {
          location = json['redirect'];
        }

        if (json['success']) {

	if(Object.keys(json['success']).length == 1) {

          $('.breadcrumb').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
          } 

	else  { $('.breadcrumb').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + 'Success: You have successfully added items to your <a href="http://easasoft.com/stapleplus/index.php?route=checkout/cart">shopping cart</a>!'+ '<button type="button" class="close" data-dismiss="alert">&times;</button></div>'); } 

          // Need to set timeout otherwise it wont update the total
          setTimeout(function () {
    
          }, 100);
      
          $('html, body').animate({ scrollTop: 0 }, 'slow');

          $('#cart > ul').load('index.php?route=common/cart/info ul li');
        }
      }
    });


});



$('.wishlist').on('click',function(){

// $.each($("input[name='product_id[]']"), function() {
//   if($(this).val()!=''){
//   product_id.push($(this).val());  
// }
// });

    $.ajax({
      url: 'index.php?route=account/wishlist/addnew',
      type: 'post',
      data:  $('form#orderbyitem').serialize(),
      dataType: 'json',
      success: function(json) {
        $('.alert').remove();

        if (json['success']) {
          alert($('form#orderbyitem').serialize());
          $('.breadcrumb').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }

        if (json['info']) {
          $('.breadcrumb').after('<div class="alert alert-info"><i class="fa fa-info-circle"></i> ' + json['info'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }

        $('#wishlist-total span').html(json['total']);
        $('#wishlist-total').attr('title', json['total']);

        $('html, body').animate({ scrollTop: 0 }, 'slow');
      }
    });

 });




var proid= new Array();
var proqty= new Array();

$("input[name='quantity[]']").change(function(){ 
  proqty.push($(this).val());
//  alert($(this).val());
});


$(document).on("change",'input[name="product_id[]"]', function () {
  var $this=$(this);
$.ajax({
type: "post",
url: "index.php?route=product/ajaxcall",
dataType: "json",         
data: 'proid='+$(this).val()+'&proqty='+proqty,            
success: function(json) {
if(json['res']) {


var obi = json.res;
var resp ='';
if(obi != '') {
resp +=  '<div class="table-responsive hideitem" ><table class="table" style="border:1px solid #ccc;"><tbody><tr><td><a href="index.php?route=product/product&product_id='+obi[0].product_id+'"><img src="image/'+obi[0].image+'" width="80"></a></td>'+
'<td><a href="index.php?route=product/product&product_id='+obi[0].product_id+'">'+obi[0].name+'</a><br>Item # '+obi[0].model+'</td><td>'+parseFloat(obi[0].price).toFixed(2)+'</td><td class="close">Remove</td></tr></tbody></table></div>';
$($this).css('display','none');
}
else {  
  resp += '<div class="table-responsive hideitem" ><table class="table" style="border:1px solid #ccc;"><tbody><tr><td>Item not found</td><br><td>Item # '+$this.val()+'</td><td class="close">Remove</td></tr></tbody></table></div>';
  $($this).css('display','none');
  $($this).val();
 }
$($this).after(resp);


}

}

});
});

</script>




    

<?php echo $footer;?>
