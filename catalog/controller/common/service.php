<?php 

class ControllerCommonService extends Controller {

	public function index() {

	$this->load->model('catalog/service');
	$this->document->setTitle("Services");

	 $data['servicelist'] = $this->model_catalog_service->service();

	$data['footer']	= $this->load->Controller('common/footer');
	$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/service.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/service.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/home.tpl', $data));
		}
	}

}
?>
