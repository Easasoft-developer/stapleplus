<?php
class ControllerCoupon extends Controller {
	public function index() {


			$this->load->model('catalog/coupon');
			$this->load->model('tool/image');
			$this->document->setTitle("Coupons");

			$coupon_info = $this->model_catalog_coupon->coupon();

			foreach ($coupon_info as $key=>$results) {
				if ($results['image']) {
					$coupon_info[$key]['image'] = $this->model_tool_image->resize($results['image'], 30, 30);
				} else {
					$coupon_info[$key]['image']= $this->model_tool_image->resize('placeholder.png', 30, 30);
				}
		}
			$data['coupon_info'] = $coupon_info;
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

           if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/coupon.tpl')) {
		$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/coupon.tpl', $data));
		} else {
		$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}

	}
	
	}	




