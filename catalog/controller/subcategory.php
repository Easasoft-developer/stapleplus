<?php
class ControllerSubcategory extends Controller {
	public function index() {

	$this->document->setTitle($this->language->get('heading_title'));
		
		$data['breadcrumbs'] = array(); 				// $data[] = array('breadcrumbs');

		$data['breadcrumbs'][] = array(    	// $data[] = array('breadcrumbs' => array['text'] => $this->language->get('text_home'));
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');




		$this->load->model('catalog/category');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {

			$children_data = array();

			
				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach($children as $child) {
			 	
					$this->load->model('tool/image');

					$img = empty($child['image']) ? 'no_image.png' : $child['image'];

					$thumb = $this->model_tool_image->resize($img, 150, 150);
					



					$filter_data = array('filter_category_id' => $child['category_id'], 'filter_sub_category' => true);

					$children_data[] = array(
						'category_id' => $child['category_id'],
						'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']),
						'image' => $thumb
					);
				}
			


			$data['categories'][] = array(
				'category_id' => $category['category_id'],
				'children'    => $children_data,
				'href'        => $this->url->link('product/category', 'path=' . $category['category_id'])
			);
		}






		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/subcategory.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/subcategory.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/account.tpl', $data));
		}

	}

}

?>
